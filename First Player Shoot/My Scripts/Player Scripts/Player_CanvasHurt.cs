﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement effect when the player gets hurt by bomb or enemies
*/
namespace DRDC1{
	public class Player_CanvasHurt : MonoBehaviour {
		private Player_Master myPlayer_Master;
		public GameObject hurtCanvas;
		private float secondsTillHide=2;
//set up
		void OnEnable(){
			setInitialReferences ();
			myPlayer_Master.EventPlayerHealthDeduction += TurnOnHurtEffect;

		}
//clean up
		void OnDisable(){
			myPlayer_Master.EventPlayerHealthDeduction -= TurnOnHurtEffect;
		}

		// Use this for initialization
		void Start () {

		}
//initializaiton
		void setInitialReferences (){
			myPlayer_Master = GetComponent<Player_Master> ();
		}
//turn on hurt effect when the player gets hurt
		void TurnOnHurtEffect(int dummy){
			if (hurtCanvas != null) {
				StopAllCoroutines ();
				hurtCanvas.SetActive (true);
				StartCoroutine (ResetHurtCanvas());
			}
		}
//disactivate the hurt canvas when the palyer stop being hurt
		IEnumerator ResetHurtCanvas(){
			yield return new WaitForSeconds(secondsTillHide);
			hurtCanvas.SetActive (false);
		}
	}
}