﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
implemetn player's inventory functionality
*/
namespace DRDC1{
	public class Player_Inventory : MonoBehaviour {
		public Transform inventoryPlayerParent;
		public Transform inventoryUIParent;
		public GameObject uiButton;

		private Player_Master playerMaster;
		private GameManager_ToggleInventoryUI inventoryUIScript;
		private float timeToPlaceInHand=0.1f;
		private Transform currentlyHeldItem;
		private int counter;
		private string buttonText;
		private List<Transform> listOfInventory = new List<Transform> ();
//set up
		void OnEnable(){
			SetInitialReference ();
			UpdateInventoryListAndUI ();
			CheckIfHandsEmpty ();
			DeactivateAllInventoryItems ();
			playerMaster.EventInventoryChanged += UpdateInventoryListAndUI;
			playerMaster.EventInventoryChanged += CheckIfHandsEmpty;
			playerMaster.EventHandsEmpty += ClearHands;
		}
//clean up
		void OnDisable(){
			playerMaster.EventInventoryChanged -= UpdateInventoryListAndUI;
			playerMaster.EventInventoryChanged -= CheckIfHandsEmpty;
			playerMaster.EventHandsEmpty -= ClearHands;
		}
//initializaiotn
		void SetInitialReference(){
			playerMaster = GetComponent<Player_Master> ();
			inventoryUIScript = GameObject.Find ("GameManager").GetComponent<GameManager_ToggleInventoryUI>();

		}
		//update invertory and its UI
		void UpdateInventoryListAndUI(){
			counter = 0;
			listOfInventory.Clear ();
			listOfInventory.TrimExcess ();

			ClearInventoryUI ();

			foreach (Transform child in inventoryPlayerParent) {
				if(child.CompareTag("Item")){
				listOfInventory.Add (child);
				GameObject go = Instantiate (uiButton) as GameObject;
				buttonText = child.name;
				go.GetComponentInChildren < Text >().text= buttonText;
				int index = counter;
				go.GetComponent<Button>().onClick.AddListener(delegate {ActivateInventoryItem(index);});
				go.GetComponent<Button>().onClick.AddListener(inventoryUIScript.ToggleInventoryUI);	
				go.transform.SetParent (inventoryUIParent); 
				counter++;
			}
		}

	}
	//check if there is nothing in the hand of player		
	void CheckIfHandsEmpty(){
		if (currentlyHeldItem == null && listOfInventory.Count > 0) {
			StartCoroutine (PlaceItemInHand (listOfInventory [listOfInventory.Count - 1]));
		}

	}
//place nothing in player's hand
	void ClearHands(){
		currentlyHeldItem = null;
	}
//destroy inventory's item
	void ClearInventoryUI(){
		foreach (Transform child in inventoryUIParent) {
			Destroy (child.gameObject);
		}

	}
//pick item from inverntory to place in player's hand
	public void ActivateInventoryItem(int inventoryIndex){
		DeactivateAllInventoryItems ();
		StartCoroutine (PlaceItemInHand (listOfInventory [inventoryIndex]));
	}
//put the item back to the invertory from player's hand
	void DeactivateAllInventoryItems(){
		foreach (Transform child in inventoryPlayerParent) {
			if(child.CompareTag("Item")){
				child.gameObject.SetActive(false);
			}
		}
	}
//place item in player's hand
	IEnumerator PlaceItemInHand(Transform itemTransform){
		yield return new WaitForSeconds (timeToPlaceInHand);
		currentlyHeldItem = itemTransform;
		currentlyHeldItem.gameObject.SetActive (true);

	}
}
}