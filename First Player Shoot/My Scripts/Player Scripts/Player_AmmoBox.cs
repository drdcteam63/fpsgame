﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
set up player's ammo box
*/
namespace DRDC1{
public class Player_AmmoBox : MonoBehaviour {
		private Player_Master playerMaster;

		[System.Serializable]
		public class AmmoTypes
		{
			public string ammoName;
			public int ammoCurrentCarried;//current ammo in magzine
			public int ammoMaxQuantity;//max ammo can be carried
//struct for ammo types[in current version of game, we only have one kind of ammo]
			public AmmoTypes(string aName, int aCurrentCarried, int aMaxQuantity){
				ammoName=aName;
				ammoCurrentCarried=aCurrentCarried;
				ammoMaxQuantity=aMaxQuantity;
			}

		}

		public List<AmmoTypes> typesOfAmmunition=new List<AmmoTypes>();
//set up
		void OnEnable(){
			SetInitialReference ();
			playerMaster.EventPickedUpAmmo += PickedUpAmmo;

		}
//clean up
		void OnDisable(){
			playerMaster.EventPickedUpAmmo -= PickedUpAmmo;
		}
//initialization
		void SetInitialReference(){
			playerMaster = GetComponent<Player_Master> ();

		}
//update carried ammo and call ammo change event
		void PickedUpAmmo(string ammoName, int quantity){
			for (int i = 0; i < typesOfAmmunition.Count; i++) {
				if (typesOfAmmunition [i].ammoName == ammoName) {
					typesOfAmmunition [i].ammoCurrentCarried += quantity;
					if (typesOfAmmunition [i].ammoCurrentCarried > typesOfAmmunition [i].ammoMaxQuantity) {
						typesOfAmmunition [i].ammoCurrentCarried = typesOfAmmunition [i].ammoMaxQuantity;
					}
					playerMaster.CallEventAmmoChanged ();
					break;

				}
			}

		}

	
}
}
