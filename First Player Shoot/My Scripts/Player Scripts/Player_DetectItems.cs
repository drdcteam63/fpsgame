﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement the functionalities that the player can detect item
*/
namespace DRDC1{
public class Player_DetectItems : MonoBehaviour {
	public LayerMask layerToDetect;//items on which layer can be detected by palyer
	public Transform rayTransformPivot;
	public string buttomPickUp;//button for picking up

	private Transform itemsAvailableForPickUp;
	private RaycastHit hit;
	private float detectRange = 3;//detection range for player
	private float detectRadius = 0.7f;//detection range's radius for player
	private bool itemInRange;//tell if the item is in range

	private float labelWidth = 200;//when the item is detected , a label will be displayed alongside the item
	private float labelHeight = 50;
    public GameObject attentioncanvas;
        public GameObject nbackcanvas;
    public AttentionControlScript attentioncontrol;
        public NbackScript nback;
    public GameObject Gamemanager;

	// Update is called once per frame, send raycast to detect items for palyer
	void Update () {
		CastRayForDetectedItems ();
		CheckForItemPickUpAttempt ();


    }
//cast ray to ddetect items
	void CastRayForDetectedItems(){
		if(Physics.SphereCast(rayTransformPivot.position,detectRadius,rayTransformPivot.forward,out hit, detectRange,layerToDetect)){
			itemsAvailableForPickUp=hit.transform;
			itemInRange=true;
		}
			else{
				itemInRange=false;
			}

	}
//check if the pick up button is pressed down 
	void CheckForItemPickUpAttempt(){
			if (PickUpControl.ispickupbutton && itemInRange&& itemsAvailableForPickUp.root.tag != GameManager_References._playerTag && Time.timeScale > 0) {
				//Debug.Log (itemsAvailableForPickUp.name);
				itemsAvailableForPickUp.GetComponent<Item_Master> ().CallEventPickupAction (rayTransformPivot);
		}

	}
//check if bombs for attention control and n-bck are detected
    public void CheckForItemBombAttemp()
        {
            if(itemInRange && itemsAvailableForPickUp.root.tag =="Bomba" && Time.timeScale > 0)
            {
                //Debug.Log("come on come on");
                attentioncontrol=Gamemanager.GetComponent<AttentionControlScript>();
                attentioncontrol.bombitem = itemsAvailableForPickUp.gameObject;
                attentioncontrol.startcanvas();

            }
            else if (itemInRange && itemsAvailableForPickUp.root.tag == "Bombb" && Time.timeScale > 0)
            {
                //Debug.Log("come on come on");
                nback = Gamemanager.GetComponent<NbackScript>();
                nback.bombitem = itemsAvailableForPickUp.gameObject;
                nback.startcanvas();

            }
        }

        //set up label for items on GUI
        void OnGUI()
        {
            if (itemInRange && itemsAvailableForPickUp != null && attentioncanvas.active == false && nbackcanvas.active == false)
            {
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.black;

                GUI.Label(new Rect(Screen.width / 2 - labelWidth / 2, Screen.height / 2, labelWidth, labelHeight), itemsAvailableForPickUp.name, style);
            }
            /*
            else if(itemInRange && itemsAvailableForPickUp != null && nbackcanvas.active == false)
                {
                    GUI.Label(new Rect(Screen.width / 2 - labelWidth / 2, Screen.height / 2, labelWidth, labelHeight), itemsAvailableForPickUp.name);
                    return;
                }
    */
        }
}
}
