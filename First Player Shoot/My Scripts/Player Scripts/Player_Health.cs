﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
implement player health change
*/
namespace DRDC1{
public class Player_Health : MonoBehaviour {
	private GameManager_Master myGameManager_Master;
	private Player_Master myPlayer_Master; 
	public int playerHealth;
	public Text healthText;
        private SelfTalk selftalk;
        private SelfTalkScene2 selftalkscene2;
//set up
		void OnEnable(){
			setInitialReference ();
			SetUI ();
			myPlayer_Master.EventPlayerHealthDeduction += DeductHealth;
			myPlayer_Master.EventPlayerHealthIncrease += IncreaseHealth;

		}
//diable
		void OnDisable(){
			myPlayer_Master.EventPlayerHealthDeduction -= DeductHealth;
			myPlayer_Master.EventPlayerHealthIncrease -= IncreaseHealth;
		}

		// Use this for initialization
		void Start () {
			//StartCoroutine (TestHealthDeduction ());
		}
//initializtion
		void setInitialReference(){
			myGameManager_Master = GameObject.Find ("GameManager").GetComponent<GameManager_Master> ();
			myPlayer_Master = GetComponent<Player_Master> ();
            if(GameObject.Find("GameManager").GetComponent<SelfTalk>()!=null)
            selftalk = GameObject.Find("GameManager").GetComponent<SelfTalk>();
            else
            {
                selftalkscene2 = GameObject.Find("GameManager").GetComponent<SelfTalkScene2>();
            }

        }

//test if the player's health is deducted, call related event
		IEnumerator TestHealthDeduction(){
			yield return new WaitForSeconds(2);
			//DeductHealth (50);
			myPlayer_Master.CallEventPlayerHealthDeduction(50);

		}

//deduct player's health
		void DeductHealth(int healthChange){
			playerHealth -= healthChange;
			if (playerHealth <= 0) {
				playerHealth = 0;
                Debug.Log("low health");
				myGameManager_Master.CallEventGameOver ();
				}
            if(playerHealth<=50&&playerHealth>=1)
            {
                //Debug.Log("low healthhhh");
                if(selftalk!=null)
                selftalk.showselftalk();
                else
                {
                    selftalkscene2.showselftalk();
                }
            }
			SetUI ();
		}
//increase palyer's health
		void IncreaseHealth(int healthChange){
			playerHealth += healthChange;
			if (playerHealth >= 100) {
				playerHealth = 100;
			}
			SetUI ();

		}
//set health UI 
		void SetUI(){
			if (healthText != null) {
				healthText.text = playerHealth.ToString ();
			}

		}
}

}
