﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * implementing the First Player Controler, replacing the default version by unity
 */
namespace DRDC1
{
    public class PlayerMotor : MonoBehaviour
    {

        public float moveSpeed = 5.0f;//the speed of moving
        public float drag = 0.5f;
        public float terminalRotationSpeed = 25.0f;
        public float gravity = 20.0f;//graviaty force
        public float jumpspeed = 8.0f;//jump's speed
        public Vector3 MoveVector { set; get; }
        public Vector3 AngleVector { set; get; }
        public Vector3 moveDirection = Vector3.zero;
        public VirtualJoystick PosJoystick;
        public VirtualJoystick AngleJoystick;

        public float sensitivityX;//the speed of aiming




        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            // View angle rotation
            AngleVector = AngleInput();
           // Debug.Log(transform.eulerAngles);
            //Debug.Log(transform.localEulerAngles);
           // Debug.Log(transform.localRotation);
           // Debug.Log(transform.localRotation);
            if (transform.eulerAngles.x - AngleVector.z * sensitivityX <= 30 || transform.eulerAngles.x - AngleVector.z * sensitivityX >= 330)//moving angle for aiming, the angle is locked to (30 degree, -30 degree)
                transform.rotation = Quaternion.Euler(transform.eulerAngles.x - AngleVector.z * sensitivityX, transform.eulerAngles.y + AngleVector.x * sensitivityX, transform.eulerAngles.z);
            //transform.Rotate(-AngleVector.z * sensitivityX, AngleVector.x * sensitivityX, 0, Space.Self);
            //transform.Rotate(0, AngleVector.x * sensitivityX, 0, Space.Self);
            //transform.Rotate(-AngleVector.z * sensitivityX, 0, 0, Space.Self);
            //transform.Rotate(0, AngleVector.x * sensitivityX, 0);
            CharacterController controller = GetComponent<CharacterController>();

            moveDirection.y -= gravity * Time.deltaTime;//return to earch after jump
            // Movement control
            MoveVector = PoolInput();
            MoveVector = transform.TransformDirection(MoveVector);
            controller.Move(MoveVector * Time.deltaTime * moveSpeed);
            controller.Move(moveDirection * Time.deltaTime);

        }

        // Movement joystick input
        private Vector3 PoolInput()
        {
            Vector3 dir = Vector3.zero;

            //if (PosJoystick == null)
            //Debug.Log("where is it");
            //Debug.Log(PosJoystick.Horizontal());
            dir.x = PosJoystick.Horizontal();
            dir.z = PosJoystick.Vertical();

            if (dir.magnitude > 1)
                dir.Normalize();

            return dir;
        }

        // Angle joystick input
        private Vector3 AngleInput()
        {
            Vector3 dir = Vector3.zero;

            //if (PosJoystick == null)
            //Debug.Log("where is it");
            //Debug.Log(AngleJoystick.Horizontal());
            dir.x = AngleJoystick.Horizontal();
            dir.z = AngleJoystick.Vertical();

            if (dir.magnitude > 1)
                dir.Normalize();

            return dir;
        }
        public void JumpInput()//Jump Input
        {
            CharacterController controller = GetComponent<CharacterController>();
            if (controller.isGrounded == true)
            {
                moveDirection.y = jumpspeed;
            }

            Debug.LogWarning("jumptest");
        }

    }

}

