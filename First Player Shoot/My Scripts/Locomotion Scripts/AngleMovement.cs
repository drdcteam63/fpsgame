﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * don't need any more
 */
namespace DRDC1
{
    public class AngleMovement : MonoBehaviour
    {
        private int count = 0;
        private Vector2 inputVector;
        public Vector3 AngleVector { set; get; }
        // Use this for initialization

        // Update is called once per frame
        void Update()
        {
            Debug.LogWarning("angle detect");
            Debug.LogWarning("count"+Input.touchCount);
            if(Input.touchCount>0&&Input.GetTouch(0).phase==TouchPhase.Moved)
            {
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                transform.Rotate(0, AngleVector.x * 2.5f, 0);
            }
            
        }

        Vector3 AngleMovementRequest()
        {
            Vector3 dir = Vector3.zero;

            dir.x = inputVector.x;
            dir.z = inputVector.y;

            if (dir.magnitude > 1)
                dir.Normalize();

            return dir;
        }
    }
}


