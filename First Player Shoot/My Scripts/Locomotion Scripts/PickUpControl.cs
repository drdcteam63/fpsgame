﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/*
 * control the pick-up button on the screen
 */
namespace DRDC1
{
    public class PickUpControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        public static bool ispickupbutton = false;
        public void OnDrag(PointerEventData eventData)
        {

        }

        public void OnPointerDown(PointerEventData eventData)
        {
            ispickupbutton = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ispickupbutton = false;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
