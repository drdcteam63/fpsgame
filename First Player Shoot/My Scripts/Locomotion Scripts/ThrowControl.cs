﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/*
 * control the throw button on the screen
 */
namespace DRDC1
{
    public class ThrowControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        public static bool isthrowbutton = false;

        public void OnDrag(PointerEventData eventData)
        {

        }

        public void OnPointerDown(PointerEventData eventData)
        {
            isthrowbutton = true;
            //Debug.Log("throwtrue");
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            isthrowbutton = false;
        }

        // Use this for initialization
        void Start()
        {
            SetInitialReferences();
        }

        void SetInitialReferences()
        {

        }
        // Update is called once per frame
        void Update()
        {

        }


    }
}