﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * don't need any more
 */
namespace DRDC1
{
    public class VirtualJumpstick : MonoBehaviour, IPointerUpHandler, IPointerDownHandler,IPointerClickHandler
    {
        public Image JumpImg;
        private Vector3 inputVector;
        Vector3 dir = Vector3.zero;
        public float speed = 6.0f;
        public float jumpSpeed = 8.0f;
        public float gravity = 20.0f;

        private Vector3 moveDirection = Vector3.zero;
        private bool grounded = false;

        // Use this for initialization
        void Start()
        {
            JumpImg = GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        public virtual void OnPointerDown(PointerEventData ped)
        {
            jump(ped);
        }

        public virtual void OnPointerUp(PointerEventData ped)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        public void jump(PointerEventData ped)
        {
            Debug.LogWarning("jump");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.LogWarning("jump");
        }
    }
}


