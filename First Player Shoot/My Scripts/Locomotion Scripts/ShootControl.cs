﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
/*
 * control the shoot button in the screen
 */
namespace DRDC1{
    public class ShootControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        private Gun_StandardInput GunInput;
        public bool isshootbutton = false;

        public void OnPointerDown(PointerEventData eventData)
        {
            //btn.OnPointerDown(eventData);
            isshootbutton = true;
            //Debug.Log(isshootbutton);
            //GunInput.Update();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            //btn.OnPointerUp(eventData);
            isshootbutton = false;
            //Debug.Log(isshootbutton);
            //GunInput.Update();
        }

        // Use this for initialization
        void Start()
        {
            //btn = ShootButton.GetComponent<Button>();
            SetInitialReferences();


        }
        void SetInitialReferences()
        {
            GunInput = new Gun_StandardInput();
        }
        // Update is called once per frame
        void Update()
        {

        }

        public void OnDrag(PointerEventData eventData)
        {
            
        }
    }
}

