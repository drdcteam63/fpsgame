﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
/*
 * change scene
 */
namespace DRDC1
{
    public class MoveScene : MonoBehaviour
    {

        [SerializeField]private string loadlevel;
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Player"))
            {
                SceneManager.LoadScene(loadlevel);//change scene
            }
        }
    }
}


