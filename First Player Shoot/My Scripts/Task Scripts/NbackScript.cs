﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

/*
 * implementing the function of Dual-2back
 */
namespace DRDC1 {
public class NbackScript : MonoBehaviour {

    public GameObject nbackcanvas;
        public GameObject bombitem;
        public GameObject Gamemanager;
        private Destructible_Master destructibleMaster;
        private int[] difficultys= {5,7,9 };// pre-set difficulty values array
        private int difficulty=9;//difficulty from goal-setting page
        GameObject panel;
    float timePassed = 0f;
        int positioncheck=0;
        int userposition = 0;
        int usersoundindex = 0;
        int soundcheck = 0;
    public AudioClip[] sounds;//store the test audio files
    private Transform myTransform;
    public GameObject attachposition;
        /*attention true*/
    private bool continuemark = false;//bool for checking position
    private bool soundcontinuemark = false;//bool for checking sound
    public float shootVolume = 0.4f;//the volume of playing audios
    private float checkRate = 2f;//time length before last update
        private float nextCheck;//next time for checking update
        float rateOfChange = 10f;
    private bool toogle = false;
    int count = -1;
    int[] positions = { -150, 0, 150 };//positions for producting red cubes
    List<int> rannumbers = new List<int>();
    int[,] cubepositions;
        int[] soundindex;
        // Use this for initialization
    void Start () {

            if (PlayerPrefs.GetInt("Difficultyvalue") == 0)
            {
                setdifficulty(PlayerPrefs.GetInt("Difficultyvalue"));
            }
            panel = new GameObject("cube");
            panel.AddComponent<CanvasRenderer>();
            Image img = panel.AddComponent<Image>();
            img.color = Color.red;
        }
	
	// Update is called once per frame
    public void setdifficulty(int num)
        {
            cubepositions = new int[difficultys[num], 2];
            soundindex = new int[difficultys[num]];
            difficulty = difficultys[num];
            //Debug.Log(difficulty);
        }
	void Update () {

            
              //comparing correct results with player's responce
             
            if (count > 1 && count < difficulty)
            {
                /*attention delete*/
                continuemark = false;
                soundcontinuemark =false;
                if (cubepositions[count, 0] == cubepositions[count - 2, 0] && cubepositions[count, 1] == cubepositions[count - 2, 1]&&soundindex[count-2]==soundindex[count])
                {
                    positioncheck = 2;
                    soundcheck = 2;
                }
                else if(cubepositions[count, 0] == cubepositions[count - 2, 0] && cubepositions[count, 1] == cubepositions[count - 2, 1] && soundindex[count - 2] != soundindex[count])
                {
                    positioncheck = 2;
                    soundcheck = 1;
                }
                else if(cubepositions[count, 0] != cubepositions[count - 2, 0] && cubepositions[count, 1] == cubepositions[count - 2, 1] && soundindex[count - 2] == soundindex[count])
                {
                    positioncheck = 1;
                    soundcheck = 2;
                }
                else if (cubepositions[count, 0] == cubepositions[count - 2, 0] && cubepositions[count, 1] != cubepositions[count - 2, 1] && soundindex[count - 2] == soundindex[count])
                {
                    positioncheck = 1;
                    soundcheck = 2;
                }
                else if (cubepositions[count, 0] != cubepositions[count - 2, 0] && cubepositions[count, 1] == cubepositions[count - 2, 1] && soundindex[count - 2] != soundindex[count])
                {
                    positioncheck = 1;
                    soundcheck = 1;
                }
                else if (cubepositions[count, 0] != cubepositions[count - 2, 0] && cubepositions[count, 1] == cubepositions[count - 2, 1] && soundindex[count - 2] != soundindex[count])
                {
                    positioncheck = 1;
                    soundcheck = 1;
                }
                else if (cubepositions[count, 0] != cubepositions[count - 2, 0] && cubepositions[count, 1] != cubepositions[count - 2, 1] && soundindex[count - 2] == soundindex[count])
                {
                    positioncheck = 1;
                    soundcheck = 2;
                }
                else if (cubepositions[count, 0] != cubepositions[count - 2, 0] && cubepositions[count, 1] != cubepositions[count - 2, 1] && soundindex[count - 2] != soundindex[count])
                {
                    positioncheck = 1;
                    soundcheck = 1;
                }
                if (userposition + 1 == positioncheck)
                {
                    continuemark = true;
                    //soundcontinuemark = true;
                }
                if(usersoundindex+1==soundcheck)
                {
                    soundcontinuemark = true;
                }

            }
            if(count>= difficulty)
            {
                successclosecanvas();
            }
            if (Time.realtimeSinceStartup > nextCheck&&toogle==true)//updating the UI
            {
                
                if (continuemark==true&&soundcontinuemark==true)
                {
                    //Debug.Log("next check");
                    nextCheck = Time.realtimeSinceStartup + checkRate;
                    if (count < difficulty-1)
                    {
                        count++;
                        displaycubes(count);
                        PlaySounds(count);
                        userposition = 0;
                        positioncheck = 0;
                        usersoundindex = 0;
                        soundcheck = 0;


                        
                    }
                    else
                    {
                        successclosecanvas();
                    }
                }
                else
                {
                    closecanvas();
                }
               
            }
              
	}
        public void startcanvas()//open canvas
        {
            destructibleMaster = bombitem.GetComponent<Destructible_Master>();
            continuemark = true;
            soundcontinuemark = true;
            myTransform = attachposition.transform;

            //Randomnumber(3);
            Randomsoundindex();
            Randomposition();
            nbackcanvas.SetActive(true);
            toogle = true;

            /*
            for (int i = 0; i < 9; i++)
            {
                displaycubes(i);
                Thread.Sleep(1000);
            }
            */
        } 

        public void closecanvas()//close canvas when defusing bombs failed
        {
            //Debug.Log(continuemark);
            //Debug.Log(soundcontinuemark);
            nbackcanvas.SetActive(false);
            //panel = null;
            continuemark = false;
            soundcontinuemark = false;
            toogle = false;
            userposition = 0;
            positioncheck = 0;
            count = -1;
            destructibleMaster.CallEventDeductHealth(100);
        }
        public void successclosecanvas()//close canvas when defusing bombs successfully
        {
            nbackcanvas.SetActive(false);
            //panel = null;
            continuemark = false;
            soundcontinuemark = false;
            toogle = false;
            userposition = 0;
            positioncheck = 0;
            count = -1;
            bombitem.SetActive(false);
            if(Gamemanager.GetComponent<GameManager_GameGoals>()!=null)
            Gamemanager.GetComponent<GameManager_GameGoals>().bombBdefusedsuccess();
            else
            {
                Gamemanager.GetComponent<GameManager_GameGoalsScene2>().bombBdefusedsuccess();
            }
        }
        void displaycubes(int i)//display the cubes
        {
            //Thread.Sleep(1000);
            panel.transform.SetParent(null, false);
                panel.transform.position = new Vector3(cubepositions[i,0], cubepositions[i,1], 0);
                panel.transform.SetParent(nbackcanvas.transform, false);
         

        }
        void Randomnumber(int n)
        {
            n = 3;
            for(int i=0;i<n;)
            {
                int t = Random.Range(0, difficulty);
                if(!rannumbers.Contains(t))
                {
                    i++;
                    rannumbers.Add(t);
                }
            }

        }

        void Randomposition()//produce the sequence of cubes'position randomly
        {
            for(int i=0;i< difficulty; i++)
            {
                cubepositions[i,0] = positions[Random.Range(0, 3)];
                cubepositions[i, 1] = positions[Random.Range(0, 3)];
                //cubepositions[i,0] = positions[0];
                //cubepositions[i, 1] = positions[0];
            }
        }

        void Randomsoundindex()//produce the sequence of audio randomly
        {
            for(int i=0;i< difficulty; i++)
            {
                soundindex[i] = Random.Range(0, 6);
                //soundindex[i] = 0;
            }
        }

        void PlaySounds(int i)//play audio
        {
                AudioSource.PlayClipAtPoint(sounds[soundindex[i]], myTransform.position, shootVolume);
        }

        public void setcontinuemaek()//get position responce from player
        {
            if (count >= 2)
                userposition = 1;
        }

        public void setsoundcontinuemark()//get sound responce from player
        {
            if (count >= 2)
                usersoundindex = 1;
        }
    }
}
