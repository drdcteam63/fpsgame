﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * implementing the function of tactical breath
 */
namespace DRDC1
{
    public class TechnicalBreathing : MonoBehaviour
    {
        public GameObject TBimage;
        private bool toogle = false;
        public Sprite[] imagesources;//the materials of heart amination
        private int index=0;
        private bool direction=true;
		private float checkRate;//time length before last update
        private float nextCheck = 0;//next time for checking update
        public GameObject map;
        private int heartratedata;//if test on pc, dummy data needed
		public GameObject TacticalBreathPanel;
		private HealthKitTest hktObject;
        private bool automark=false;
        private GoalSetting goalsetting;
        private int heartratevalue=120;//the threshold for begining a tactical breath
        // Use this for initialization
        void Start()
        {
            if (map.CompareTag("Level2Scene"))//checking different scene and choosing different initialization way
            {
                heartratevalue = PlayerPrefs.GetInt("HeartRateValue");
            }

        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log(heartratevalue);
            //heartratedata = 121;
            //controling the display speed of the animation
            if (heartratedata >=170 && heartratedata <= 200)
				checkRate = 0.1f;
			if (heartratedata >= 40 && heartratedata <= 50)
				checkRate = 1f;
			if (heartratedata > 50 && heartratedata < 170)
				checkRate = 0.1f + (200f - heartratedata) * 0.7f / 160f;
            if(heartratedata>= heartratevalue)//open tactical breath panel and forcing player to do tactical breath
            {
                startTBpanel();
                automark = true;
            }
            if(heartratedata<= heartratevalue - 30&& automark==true)
            {
                closeTBPanelAuto();
                automark = false;
            }
            if(toogle==true)//if the tactical breath panel is actived, the toogle is set to true
            {

                if(Time.realtimeSinceStartup > nextCheck)
                {
                   
                    TBimage.GetComponent<Image>().sprite = imagesources[index];
                    if (direction == true)
                    {
                        if (index < 4)
                            index++;
                        else
                        {
                            index--;
                            direction = false;
                        }
                    }

                    else
                    {
                        if (index > 0)
                            index--;
                        else
                        {
                            direction = true;
                            index++;
                        }
                    }
                    nextCheck = Time.realtimeSinceStartup + checkRate;
                }
                

            }
        }

		public void receivedata(int hrdata)//receiving data from heart rate monitor
		{
			heartratedata = hrdata;
		}

        public void setheartrate()//initialization 
        {
            goalsetting = this.GetComponent<GoalSetting>();
            if (goalsetting.HeartRateValueGetter() != 0)
            {
                heartratevalue = goalsetting.HeartRateValueGetter();
                //Debug.Log(heartratevalue);
            }
        }
		//start tactical breathing panel when hit the threathold
		public void startTBpanel()//opening tactical breath panel
		{
			TacticalBreathPanel.SetActive(true);
			toogle = true;
		}

		public void closeTBpanel()//closing tactical breaath panel
		{
            if(automark==false)
			TacticalBreathPanel.SetActive(false);
		}

        void closeTBPanelAuto()//closing tactical breaath panel automatically
        {
            TacticalBreathPanel.SetActive(false);
        }
		public float getCheckRate(){
		
			return checkRate;
		}

    }



}
