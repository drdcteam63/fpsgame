﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * implementing the function of task information(insturction), panel needed.
 */

namespace DRDC1
{
    public class TaskInformation : MonoBehaviour
    {
		public GameObject notice_canvas;


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
		public void startcanvas()
		{
			notice_canvas.SetActive(true);
		}

		public void closecanvas()
		{
			notice_canvas.SetActive(false);
		}

    }
}

