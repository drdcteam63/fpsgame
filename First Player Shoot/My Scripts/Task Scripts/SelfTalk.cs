﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * implementing the function of self-talk in Game Scene
 */
namespace DRDC1
{
    public class SelfTalk : MonoBehaviour
    {
        public Text inputtext;
        public GameObject selftalkpanel;
        public Text selftalktext;
        static string text;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {
            transmit();
        }

        public void showselftalk()//display self-talk
        {
            selftalkpanel.SetActive(true);
            selftalktext.text = inputtext.text;

        }

        void transmit()//transmit data for another scene
        {
            text = inputtext.text;
            PlayerPrefs.SetString("selftalk", text);
        }
    }
}

