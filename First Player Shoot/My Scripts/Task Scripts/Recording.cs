﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * implementing the function of recording in visualization
 */
namespace DRDC1
{
    public class Recording : MonoBehaviour
    {

        // Use this for initialization
        private AudioClip myAudioClip;
        public GameObject Text;
        private float checkRate = 1f;//time length before last update
        private float nextCheck = 0;//next time for checking update
        private int i = 11;//length of audio
        private bool startflag = false;
        public void Startrecoding()//start recording
        {
            if(startflag==false)
            {
                myAudioClip = Microphone.Start(null, false, 10, 44100);
                startflag = true;
                Text.GetComponent<Text>().text = "Start";
                i = 11;
            }

            //aud.Play();
            //myAudioClip = aud.clip;


            Debug.Log("playing");
        }

        public void play()//play audio
        {
            AudioSource aud = GetComponent<AudioSource>();
            aud.clip = myAudioClip;
            aud.Play();
            SavWav.Save("myfile", myAudioClip);
        }
        // Update is called once per frame
        void Update()//update UI when recording
        {
            if(Time.realtimeSinceStartup > nextCheck&& startflag==true)
            {
                if(i>0)
                {
                    i--;
                    Text.GetComponent<Text>().text = i.ToString()+" s";
                    nextCheck = Time.realtimeSinceStartup + checkRate;
                }
                if(i==0)
                {
                    Text.GetComponent<Text>().text = "Finished";
                    startflag = false;
                }


            }
        }
    }
}

