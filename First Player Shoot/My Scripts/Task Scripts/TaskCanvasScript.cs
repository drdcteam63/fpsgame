﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * Read the information in  goal_setting and game and output it to taskcanvas
 */
namespace DRDC1{
public class TaskCanvasScript : MonoBehaviour {
		public GameObject taskcanvas; // Refer to tack Canvas GameObject
		public Toggle toggle1, toggle2, toggle3; // Refer to toggle game object
		private GameManager_GameGoals goal_script; // refer to goal_script script
		private GoalSetting goal_setting; // refer to goal setting script
		private GameManager_GameGoalsScene2 goal_script2; // refer to goal_script2 script in scence2
		private Text txt1, txt2, txt3; // refer to txt
		int bombacount,bombagoal,bombbcount, bombbgoal; // the number of bomb player want to generate
		int heartRateThreshold=160;  //JUST FOR NOW // the rate of hearRate player want to set
		public GameObject map; // the terrain GameObject

	// Use this for initialization
	void Start () {
			SetInitialReference ();
			toggle1.isOn = false;
			toggle2.isOn = false;
			toggle3.isOn = false;
			taskcanvas.SetActive (false);
	}

	// Update is called once per frame
	 void Update () {
			if(map.CompareTag ("Level2Scene")){
				bombacount = goal_script2.bombacountGetter();
				bombagoal = goal_script2.bombagoalGetter ();
				bombbcount = goal_script2.bombbcountGetter ();
				bombbgoal = goal_script2.bombbgoalGetter ();
				heartRateThreshold = goal_script2.heartrateGetter ();
			} else{
				heartRateThreshold = goal_setting.HeartRateValueGetter ();
				bombacount = goal_script.bombacountGetter();
				bombagoal = goal_script.bombagoalGetter ();
				bombbcount = goal_script.bombbcountGetter ();
				bombbgoal = goal_script.bombbgoalGetter ();
			}

			if (checkIfBombaComplete ()) {
				toggle1.isOn = true;
				txt1.text="BombA Goal is " +bombagoal;
			} else {
				txt1.text= "BombA Goal is " +bombagoal;
			}

			if (checkIfBombbComplete ()) {
				toggle2.isOn = true;
				txt2.text="BombB Goal is " +bombbgoal;
			} else {
				txt2.text="BombB Goal is " +bombbgoal;
			}

			txt3.text = "Heart Rate Goal " + heartRateThreshold;
		}

    void SetInitialReference(){
			if (map.CompareTag ("Level2Scene")) {
				goal_script2 = this.GetComponent<GameManager_GameGoalsScene2> ();
			} else {
				goal_setting = this.GetComponent<GoalSetting> ();
				goal_script = this.GetComponent<GameManager_GameGoals> ();
			}
			txt1=(Text) (GameObject.Find("Label1").GetComponent<Text>());
			txt2=(Text) (GameObject.Find("Label2").GetComponent<Text>());
			txt3=(Text) (GameObject.Find("Label3").GetComponent<Text>());
		}

		bool checkIfBombaComplete (){
			if (bombacount >= bombagoal) {
				return true;
			}
			return false;
		}

		bool checkIfBombbComplete (){
			if (bombbcount >= bombbgoal) {
				return true;
			}
			return false;
				}
	}
}
