﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * implementing the function of Attention control
 */
namespace DRDC1
{
    public class AttentionControlScript : MonoBehaviour
    {
        GameObject panel;
        private int difficulty; //difficulty value from Goal-Setting
        public GameObject bombitem;
        private Destructible_Master destructibleMaster;
        public GameObject watchcanvas;
        public GameObject notice;
        public GameObject Gamemanager;
        private float checkRate = 1f;//time length before last update
        private float nextCheck=0;//next time for checking update
        private int[] timelengths = {120, 180, 240 };//pre-set difficulty values array
        private int timelength=120;//time length for one task
        private int currentlength = 0;//recording how much time has passed since the task is started
        List<int> rannumbers = new List<int>();
        bool checkflag = false;//check for 
        bool checkresponse = true;//paramater for checking response from players
        float xposition=0;//pointer's x position
        float yposition=50;//pointer's y position
        int angle=0;//the roatation angle
        int count = 0;
        // Use this for initialization

        void SetInitialReferences()//initialization
        {
            destructibleMaster = bombitem.GetComponent<Destructible_Master>();

        }
        public void setdifficulty(int num)//set difficulty before task is started
        {
            difficulty = num;
            //Debug.Log(timelengths[difficulty]);
        }
        void Start()//produce a red pointer in the panel
        {
            //SetInitialReferences();
            panel = new GameObject("plane");

            panel.AddComponent<CanvasRenderer>();
            Image img = panel.AddComponent<Image>();
            img.color = Color.red;
            panel.transform.localScale = new Vector3(0.1f, 1.3f, 1.3f);
            randomnumberspawner();
        }

        // Update is called once per frame
        void Update()
        {
            if(watchcanvas.active==true)
            {
                if (nextCheck == 0)
                {
                    
                    displaypointer();
                    nextCheck = Time.realtimeSinceStartup + checkRate;
                    return;
                }
                if (Time.realtimeSinceStartup > nextCheck && currentlength < timelengths[difficulty])//update UI if time duration is longer than 1s
                {
                    if(checkresponse == false)
                    {
                        closecanvas();
                        return;
                    }
                    int factor = 1;
                    checkflag = false;
                    notice.SetActive(false);
                    if (rannumbers.Contains(currentlength))//producing skips
                    {
                        //Debug.Log("3");
                        factor = 3;
                        checkflag = true;
                        checkresponse = false;
                    }
                    panel.transform.Rotate(0, 0, -6*factor);
                    nextCheck = Time.realtimeSinceStartup + checkRate;
                    angle += 6* factor;
                    currentlength++;
                    getposition();
                    displaypointer();

                }
                if(currentlength>=timelengths[difficulty])
                {
                    successclosecanvas();
                    return;
                }
            }
            
        }

        public void startcanvas()//start canvas
        {
            //Debug.Log("pointer");
            destructibleMaster = bombitem.GetComponent<Destructible_Master>();
            watchcanvas.SetActive(true);
        }
        public void closecanvas()//close canvas when defusing bombs failed
        {
            //Debug.Log("pointer");
            watchcanvas.SetActive(false);
            nextCheck = 0;
            currentlength = 0;
            xposition = 0;
            yposition = 50;
            angle = 0;
            panel.active=false;
            Destroy(panel);
            panel = new GameObject("plane");
            checkflag = false;
            checkresponse = true;
            int count = 0;

            panel.AddComponent<CanvasRenderer>();
            Image img = panel.AddComponent<Image>();
            img.color = Color.red;
            panel.transform.localScale = new Vector3(0.1f, 1.3f, 1.3f);
            destructibleMaster.CallEventDeductHealth(100); //fail
            //bombitem.SetActive(false); //success
        }
        public void successclosecanvas()//close canvas when defusing bombs successfully
        {
            watchcanvas.SetActive(false);
            nextCheck = 0;
            currentlength = 0;
            xposition = 0;
            yposition = 50;
            angle = 0;
            panel.active = false;
            Destroy(panel);
            panel = new GameObject("plane");
            checkflag = false;
            checkresponse = true;
            int count = 0;

            panel.AddComponent<CanvasRenderer>();
            Image img = panel.AddComponent<Image>();
            img.color = Color.red;
            panel.transform.localScale = new Vector3(0.1f, 1.3f, 1.3f);
            bombitem.SetActive(false);
            Gamemanager.GetComponent<GameManager_GameGoals>().bombAdefusedsuccess();
        }
        void displaypointer()
        {
            //Thread.Sleep(1000);
            //Debug.Log("xposition"+xposition);
            //Debug.Log("yposition"+yposition);
            //Debug.Log("angle" + angle);
            panel.transform.SetParent(null, false);
            panel.transform.position = new Vector3(xposition, yposition, 0);
            panel.transform.SetParent(watchcanvas.transform, false);


        }

        void getposition()//calculating the postion of pointer
        {
            if(angle>=0&&angle<=90)
            {
                //Debug.Log("first");
                xposition = 50 * Mathf.Sin(angle * (Mathf.PI/180));
                yposition = 50 * Mathf.Cos(angle * (Mathf.PI / 180));
                return;
            }
            else if(angle>90&&angle<=180)
            {
                xposition = 50 * Mathf.Cos((angle - 90) * (Mathf.PI / 180));
                yposition = -50* Mathf.Sin((angle - 90) * (Mathf.PI / 180));
                return;
            }
            else if(angle>180&&angle<=270)
            {
                xposition = -50 * Mathf.Sin((angle-180) * (Mathf.PI / 180));
                yposition = -50 * Mathf.Cos((angle-180) * (Mathf.PI / 180));
                return;
            }
            else if(angle>270&&angle<=360)
            {
                xposition = -50 * Mathf.Cos((angle - 270) * (Mathf.PI / 180));
                yposition = 50 * Mathf.Sin((angle - 270) * (Mathf.PI / 180));
                if(angle==360)
                {
                    angle = 0;
                }
                return;
            }

        }

        void randomnumberspawner()//producing skips randomly
        {
            for(int i=0;i<6;)
            {
                int temp = Random.Range(1, 120);
                if(!rannumbers.Contains(temp))
                {
                    rannumbers.Add(temp);
                    i++;
                }
            }
        }

        public void checkresponce()//Checking responce from player
        {
            if(checkflag==true)
            {
                checkresponse = true;
                notice.SetActive(true);
            }
            
        }
    }
}

