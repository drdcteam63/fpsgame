﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * implementing the function of goal-setting
 */
namespace DRDC1
{
    public class GoalSetting : MonoBehaviour
    {
        public Dropdown Heartrate;
        public Dropdown Bomba;
        public Dropdown Bombb;
        public Dropdown Difficulty;
        private int Heartratevalue;
        private int Bombavalue;
        private int Bombbvalue;
        static private int Difficultyvalue;
        private AttentionControlScript ats;
        private NbackScript nback;
        private TechnicalBreathing tbobkject;
        private GameManager_GameGoals gamegoal;
		static private int numberOfEnemy;
		static private int numberOfBomba;
		static private int numberOfBombb;
		public GameObject BombaInput;
		public GameObject BombbInput;
		public GameObject EnemyNumberInput;
        
        // Use this for initialization
        void Start()
        {
			
        }

        // Update is called once per frame
        void Update()
        {
            datatransmit();
        }
        public void test()
        {
            //Debug.Log(Difficulty.GetComponent<Dropdown>().value);
            //Debug.Log(Difficulty.options[0].text);
        }

        public void getgoalsetting()//transmit data to different task
        {
            gamegoal = this.GetComponent<GameManager_GameGoals>();
            nback = this.GetComponent<NbackScript>();
            ats = this.GetComponent<AttentionControlScript>();
            tbobkject = this.GetComponent<TechnicalBreathing>();
            gamegoal.setbombA(Convert.ToInt32(Bomba.options[Bomba.GetComponent<Dropdown>().value].text));
            gamegoal.setbombB(Convert.ToInt32(Bombb.options[Bombb.GetComponent<Dropdown>().value].text));
            nback.setdifficulty(Convert.ToInt32(Difficulty.GetComponent<Dropdown>().value));
            ats.setdifficulty(Convert.ToInt32(Difficulty.GetComponent<Dropdown>().value));
            
            numberOfEnemy = Convert.ToInt32(EnemyNumberInput.GetComponent<Text> ().text);
			numberOfBomba = Convert.ToInt32 (BombaInput.GetComponent<Text> ().text);
			numberOfBombb = Convert.ToInt32 (BombbInput.GetComponent<Text> ().text);
            Difficultyvalue = Convert.ToInt32(Difficulty.GetComponent<Dropdown>().value);
			Heartratevalue= Convert.ToInt32(Heartrate.options[Heartrate.GetComponent<Dropdown>().value].text);
            tbobkject.setheartrate();
            //Debug.LogWarning ("Bomba" + numberOfBomba);
            //Debug.LogWarning ("Enemy" + numberOfEnemy);
        }

		public int EnemyNumberGetter(){//return numberOfEnemy
			return numberOfEnemy;
		}

		public int BombaNumberGetter(){//return numberOfBomba
			return numberOfBomba;
		}

		public int HeartRateValueGetter(){//return Heartratevalue
            return Heartratevalue;
		}
			
		public void datatransmit(){//transmit data for next scene
            PlayerPrefs.SetInt("bombanumber", numberOfBomba);
			PlayerPrefs.SetInt ("bombnumber", numberOfBombb);
			PlayerPrefs.SetInt ("enemynumber", numberOfEnemy);
            PlayerPrefs.SetInt("Difficultyvalue", Difficultyvalue);
			PlayerPrefs.SetInt ("HeartRateValue",Heartratevalue);
		}

    }
}

