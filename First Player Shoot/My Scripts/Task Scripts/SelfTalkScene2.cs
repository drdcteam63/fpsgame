﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * implementing the function of self-talk in Game1 Scene
 */
namespace DRDC1
{
    public class SelfTalkScene2 : MonoBehaviour
    {
        public GameObject selftalkpanel;
        private DataReceiver datareceiver;
        public Text selftalktext;
        // Use this for initialization
        void Start()
        {
            datareceiver = this.GetComponent<DataReceiver>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void showselftalk()//display self-talk
        {
            selftalkpanel.SetActive(true);
            selftalktext.text = datareceiver.SelfTalkGetter();
        }
    }
}

