﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * change scene
 */
namespace DRDC1
{
    public class ChangeScene : MonoBehaviour
    {

        public GameObject trigger;
        private GameManager_GameGoals gamegoal;
        private SelfTalk selftalk;
        private GameManager_GameGoals goalsetting;
        // Use this for initialization
        void Start()
        {
            gamegoal = this.GetComponent<GameManager_GameGoals>();
            goalsetting = this.GetComponent<GameManager_GameGoals>();
            selftalk = this.GetComponent<SelfTalk>();
        }

        // Update is called once per frame
        void Update()
        {
            if(gamegoal.bombagoalGetter()==gamegoal.bombacountGetter()+gamegoal.bombafailcountGetter())
            {

                //Debug.Log(gamegoal.bombacountGetter());
                //Debug.Log(gamegoal.bombagoalGetter());
                //gamegoal.transmitdata();
                //selftalk.transmit();
                //goalsetting.transmitdata();
                trigger.SetActive(true);//will be involved when all bombs in Game Scene is defused
            }
        }
    }
}

