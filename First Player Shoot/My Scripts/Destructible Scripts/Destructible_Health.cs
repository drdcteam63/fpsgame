﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * control the health of bomb
 */
namespace DRDC1
{
    public class Destructible_Health : MonoBehaviour
    {

        private Destructible_Master destructibleMaster;
        public int health;
        private int startingHealth;
        private bool isExploding = false;
        public GameObject Gamemanager;


        void Start()
        {
            SetInitialReferences();
        }

        void OnEnable()
        {
            SetInitialReferences();
            destructibleMaster.EventDeductHealth += DeductHealth;
        }

        void OnDisable()
        {
            destructibleMaster.EventDeductHealth -= DeductHealth;
        }

        void SetInitialReferences()
        {
            destructibleMaster = GetComponent<Destructible_Master>();
            startingHealth = health;
        }

        void DeductHealth(int damage)
        {

            health = health - damage;
            CheckIfHealthLow();
            if (health <= 0 && !isExploding)
            {
                isExploding = true;
                destructibleMaster.CallEVentDestroyMe();
                if (Gamemanager.GetComponent<GameManager_GameGoals>() != null)
                    Gamemanager.GetComponent<GameManager_GameGoals>().bombAdefusedfail();
                else
                {
                    Gamemanager.GetComponent<GameManager_GameGoalsScene2>().bombBdefusedfail();
                }

                //Debug.Log("destory me");
            }
        }
        void CheckIfHealthLow()
        {
            if (health <= startingHealth / 2)
            {
                destructibleMaster.CallEventHealthLow();
            }
        }
    }
}
