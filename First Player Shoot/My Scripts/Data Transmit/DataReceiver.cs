﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Implementing the function of receiving data in Game 1 Scene
 */
namespace DRDC1{
public class DataReceiver : MonoBehaviour {
		int enemyNumber;
		int BombbNumber;
        string selftalk;

	// Use this for initialization
	void Awake () {
			ReceiveData ();
	}
	
	// Update is called once per frame
	void Update () {
            ReceiveData();
        }

	void ReceiveData() {
			BombbNumber=PlayerPrefs.GetInt ("bombnumber");
			enemyNumber=PlayerPrefs.GetInt ("enemynumber");
            selftalk = PlayerPrefs.GetString("selftalk");
	}

		public int EnemyNumberGetter (){
			return enemyNumber;
		}

		public int BombaNumberGetter (){
			return BombbNumber;
		}

        public string SelfTalkGetter()
        {
            return selftalk;
        }
 }
}
