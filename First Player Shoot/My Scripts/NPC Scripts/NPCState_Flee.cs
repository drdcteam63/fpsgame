﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace DRDC1{
	/*
	 * implement the state when NPC is fleeing
	 */
public class NPCState_Flee : NPCState_Interface{
	private Vector3 directionToEnemy; // Store the direction to player
	private NavMeshHit navHit;

	private readonly NPC_StatePattern npc; //Refer to NPC_StatePattern script

	public NPCState_Flee(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}
	//Update once per frame
	public void updateState(){
		CheckIfIShouldFlee ();
		CheckIfIShouldFight ();
	}
	//Change to patrol state
	public void toPatrolState(){
		KeepWalking ();
		npc.currentState = npc.patrolState;
	}
	public void toAlertState (){}
	public void toPursueState(){}
	// Chage to melee state
	public void toMeleeAttackState (){
		KeepWalking ();
		npc.currentState = npc.meleeAttackState;
	}
	public void toRangeAttackState (){}

 // Enemy check if should flee
	void CheckIfIShouldFlee(){
		npc.meshRendererFlag.material.color = Color.gray;
		Collider[] colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange, npc.myEnemyLayers);

		if (colliders.Length == 0) {
			toPatrolState ();
			return;
		}

		directionToEnemy = npc.transform.position - colliders [0].transform.position;
		Vector3 checkPos = npc.transform.position + directionToEnemy;

		if (NavMesh.SamplePosition (checkPos, out navHit, 3.0f, NavMesh.AllAreas)) {
			npc.myNavMeshAgent.destination = navHit.position;
			KeepWalking ();
		} else {
			StopWalking ();
		}
	}

 //Enemy check if should fight
	void CheckIfIShouldFight(){
		if (npc.pursueTarget == null) {
			return;
		}
		float distanceToTarget = Vector3.Distance (npc.transform.position, npc.pursueTarget.position);
		if (npc.hasMeleeAttack && distanceToTarget <= npc.meleeAttackRange) {
			toMeleeAttackState ();
		}
	}

	void KeepWalking(){
		npc.myNavMeshAgent.Resume ();
		npc.npcMaster.callEventNpcWalkAnim ();
	}

	void StopWalking(){
		npc.myNavMeshAgent.Stop ();
		npc.npcMaster.callEventNpcIdleAnim ();
	}
}
}
