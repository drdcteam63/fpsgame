﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement turning of state pattern
	 */
public class NPC_TurnOffStatePattern : MonoBehaviour {

	private NPC_Master npcMaster; // Refer to npcMaster script
	private NPC_StatePattern npcPattern; // Refer to NPC_StatePattern script

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDie += TurnOffStatePattern;
	}

	void OnDisable(){
		npcMaster.EventNpcDie -= TurnOffStatePattern;
	}


	void SetInitialReferences(){
		npcMaster = GetComponent<NPC_Master> ();
		if (GetComponent<NPC_StatePattern> () != null) {
			npcPattern = GetComponent<NPC_StatePattern> ();
		}
	}

	void TurnOffStatePattern(){
		if (npcPattern != null) {
			npcPattern.enabled = false;
		}
	}
}
}
