﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace DRDC1{
	/*
	 * implement the state when NPC is patroling
	 */
public class NPCState_Patrol : NPCState_Interface {
	private readonly NPC_StatePattern npc; // refer to NPC_StatePattern script
	private int nextWayPoint; // Next location
	private Collider[] colliders; // An array of allies colliders
	private Vector3 lookAtPoint; //the location of the target
	private Vector3 heading; // the heading direction
	private float dotProd;
        private float checkRate = 5f; // the check frequency
        private float nextCheck = 0; // the time of the next check

        public NPCState_Patrol (NPC_StatePattern npcStatePattern){
		npc=npcStatePattern;
	}


	public void updateState(){
		Look ();
		Patrol ();

	}
	public void toPatrolState(){}
	public void toAlertState (){
		npc.currentState = npc.alertState;
	}
	public void toPursueState(){}
	public void toMeleeAttackState (){}
	public void toRangeAttackState (){}

	void Look(){
		//check medium range
		colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange / 3, npc.myEnemyLayers);
		if (colliders.Length > 0) {
			VisibilityCalculations (colliders [0].transform);
			if (dotProd > 0) {
				AlertStateActions (colliders [0].transform);
				return;
			}
		}
			//checkMaxRange
			colliders = Physics.OverlapSphere (npc.transform.transform.position, npc.sightRange, npc.myEnemyLayers);
			foreach (Collider col in colliders) {
				RaycastHit hit;
				VisibilityCalculations (col.transform);
				if (Physics.Linecast (npc.head.position, lookAtPoint, out hit, npc.sightLayers)) {
				foreach (string tags in npc.myEnemyTags) {
						if (hit.transform.CompareTag (tags)) {
							if (dotProd > 0) {
								AlertStateActions (col.transform);
								return;
							}
						}
					}
				}

			}
		}

	void Patrol(){
          //  Debug.Log("i am patrol");
            npc.meshRendererFlag.material.color = Color.green;
		if (npc.myFollowTarget != null) {
			npc.currentState = npc.followState;
		}
		if (!npc.myNavMeshAgent.enabled) {
			return;
		}
        if(Time.realtimeSinceStartup < nextCheck)
            {
                if (npc.wayPoints.Length > 0)
                {
                    MoveTo(npc.wayPoints[nextWayPoint].position);
                    if (HaveReachedDestination())
                    {
                        nextWayPoint = (nextWayPoint + 1) % npc.wayPoints.Length;
                    }
                }
                else
                { //wander there are no way points
                    if (HaveReachedDestination())
                    {
                        StopWalking();
                        if (RandomWanderTarget(npc.transform.position, npc.sightRange, out npc.wanderTarget))
                        {
                            MoveTo(npc.wanderTarget);
                        }
                    }

                }

            }
            else
            {
                StopWalking();
                if (RandomWanderTarget(npc.transform.position, npc.sightRange, out npc.wanderTarget))
                {
                    MoveTo(npc.wanderTarget);
                }
                nextCheck = Time.realtimeSinceStartup + checkRate;
            }

	}

	void AlertStateActions(Transform target){
		npc.locationOfInterest = target.position;
		toAlertState ();
	}

	void VisibilityCalculations (Transform target){
		lookAtPoint = new Vector3 (target.position.x, target.position.y + npc.offset, target.position.z);
		heading = lookAtPoint - npc.transform.position;
		dotProd = Vector3.Dot (heading, npc.transform.forward);
	}

	bool RandomWanderTarget(Vector3 centre, float range, out Vector3 result){
		NavMeshHit navHit;

		Vector3 randomPoint = centre + Random.insideUnitSphere * npc.sightRange;
		if (NavMesh.SamplePosition (randomPoint, out navHit, 3.0f, NavMesh.AllAreas)) {
			result = navHit.position;
			return true;
		} else {
			result=centre;
			return false;

		}
	}

	bool HaveReachedDestination(){
		if (npc.myNavMeshAgent.remainingDistance <= npc.myNavMeshAgent.stoppingDistance && !npc.myNavMeshAgent.pathPending) {
			StopWalking ();
			return true;
		} else {
			KeepWalking ();
			return false;
		}
	}

// Move to this location
	void MoveTo(Vector3 targetPos){
		if (Vector3.Distance (npc.transform.position, targetPos) > npc.myNavMeshAgent.stoppingDistance + 1) {
			npc.myNavMeshAgent.SetDestination (targetPos);
			KeepWalking ();
		}
	}

		void KeepWalking(){
			npc.myNavMeshAgent.Resume();
		npc.npcMaster.callEventNpcWalkAnim();
		}
		void StopWalking(){
		npc.myNavMeshAgent.Stop();
		npc.npcMaster.callEventNpcIdleAnim();
		}
	}
}
