﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement NPC's animations
	 */
public class NPC_Animations : MonoBehaviour {

	private NPC_Master npcMaster;
	private Animator myAnimator;

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcAttackAnim += ActivateAttackAnimation;
		npcMaster.EventNpcWalkAnim += ActivateWalkingAnimations;
		npcMaster.EventNpcIdleAnim += ActivateIdleAnimation;
		npcMaster.EventNpcRecoveredAnim += ActivateRecoveredAnimation;
		npcMaster.EventNpcStruckAnim += ActivateStruckAnimation;
	}

	void  OnDisable(){
		npcMaster.EventNpcAttackAnim -= ActivateAttackAnimation;
		npcMaster.EventNpcWalkAnim -= ActivateWalkingAnimations;
		npcMaster.EventNpcIdleAnim -= ActivateIdleAnimation;
		npcMaster.EventNpcRecoveredAnim -= ActivateRecoveredAnimation;
		npcMaster.EventNpcStruckAnim -= ActivateStruckAnimation;
	}

	void SetInitialReferences(){
		npcMaster = GetComponent<NPC_Master> ();  // refer to npc master script

		if (GetComponent<Animator> () != null) {
			myAnimator = GetComponent<Animator> ();
		}
	}

	void ActivateWalkingAnimations(){
            if (myAnimator != null) {   // the npc animator
				if (myAnimator.enabled) {
                    //Debug.Log("Animations Walking");
                    myAnimator.SetBool (npcMaster.animationBoolPursuing, true);
				}
			}
	}

		void ActivateIdleAnimation(){
			if(myAnimator!=null){
				if(myAnimator.enabled){
				myAnimator.SetBool(npcMaster.animationBoolPursuing,false);
				}
			}
		}

	void ActivateAttackAnimation(){
		if (myAnimator != null) {
			if (myAnimator.enabled) {
				myAnimator.SetTrigger (npcMaster.animationTriggerMelee);
			}
		}
	}

	void ActivateRecoveredAnimation(){
		if (myAnimator != null) {
			if (myAnimator.enabled) {
				myAnimator.SetTrigger (npcMaster.animationTriggerRecovered);
			}
		}
	}

	void ActivateStruckAnimation(){
		if (myAnimator != null) {
			if (myAnimator.enabled) {
				myAnimator.SetTrigger (npcMaster.animationTriggerStruck);
			}
		}
	}

}
}
