﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace DRDC1{
	/*
	 * implement turning off NavMeshAgent
	 */
public class NPC_TurnOffNavMeshAgent : MonoBehaviour {

	private NPC_Master npcMaster; // Refer to npcMaster script
	private NavMeshAgent myNavMeshAgent; //Refer to the NavMeshAgent of the NPC

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDie += TurnOffNavMeshAgent;
	}

	void OnDisable(){
		npcMaster.EventNpcDie -= TurnOffNavMeshAgent;
	}



	void SetInitialReferences(){
		npcMaster = GetComponent<NPC_Master> ();
		if (GetComponent<NavMeshAgent> () != null) {
			myNavMeshAgent = GetComponent<NavMeshAgent> ();
		}
	}

	void TurnOffNavMeshAgent(){
		if (myNavMeshAgent != null) {
			myNavMeshAgent.enabled = false;
		}
	}
}
}
