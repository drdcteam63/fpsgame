﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
public interface NPCState_Interface{
	void updateState ();
	void toPatrolState();
	void toAlertState ();
	void toPursueState();
	void toMeleeAttackState ();
	void toRangeAttackState ();
}
}