﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1
{
    public class NPC_test : MonoBehaviour
    {
        private float checkRate = 0.1f;
        private float nextCheck;
        public float sightRange = 40;
        public float detectBehindRange = 5;
        public float meleeAttackRange = 7;
        public float meleeAttackDamage = 10;
        public float rangeAttackRange = 35;
        public float rangeAttackDamage = 5;
        public float rangeAttackSpread = 0.5f;
        public float attackRate = 0.4f;
        public float nextAttack;
        public float fleeRange = 25;
        public float offset = 0.4f;
        public int requiredDetectionCount = 15;

        public bool hasRangeAttack;
        public bool hasMeleeAttack;
        public bool isMeleeAttacking;

        public Transform myFollowTarget;
        [HideInInspector]
        public Transform pursueTarget;
        [HideInInspector]
        public Vector3 locationOfInterest;
        [HideInInspector]
        public Vector3 wanderTarget;
        [HideInInspector]
        public Transform myAttacker;
        [HideInInspector]

        public LayerMask sightLayers;
        public LayerMask myFriendlyLayers;
        public LayerMask myEnemyLayers;
        public string[] myEnemyTags;
        public string[] myFriendlyTags;

        public Transform[] wayPoints;
        public Transform head;
        public MeshRenderer meshRendererFlag;
        public GameObject rangeWeapon;
        public NPC_Master npcMaster;
        [HideInInspector]
        public UnityEngine.AI.NavMeshAgent myNavMeshAgent;

        public NPCState_Interface currentState;
        public NPCState_Interface captureState;
        public NPCState_Patrol patrolState;
        public NPCState_Alert alertState;
        public NPCState_Pursue pursueState;
        public NPCState_MeleeAttack meleeAttackState;
        public NPCState_RangeAttack rangeAttackState;
        public NPCState_Flee FleeState;
        public NPCState_Struck struckState;
        public NPCState_InvestigateHArm investigationHarmState;
        public NPCState_Follow followState;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
