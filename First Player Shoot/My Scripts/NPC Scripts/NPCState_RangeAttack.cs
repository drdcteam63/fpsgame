﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is doing range attack
	 */
public class NPCState_RangeAttack : NPCState_Interface {
	private readonly NPC_StatePattern npc; //refer to NPC_StatePattern script
	private RaycastHit hit;

	public NPCState_RangeAttack(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}

	public void updateState(){
		Look ();
		//TryToAttack ();
	}
	public void toPatrolState(){
		KeepWalking ();
		npc.pursueTarget = null;
		npc.currentState = npc.patrolState;
	}
	public void toAlertState (){
		KeepWalking ();
		npc.currentState = npc.alertState;
	}
	public void toPursueState(){
		KeepWalking ();
		npc.currentState = npc.pursueState;
	}
	public void toMeleeAttackState (){
		npc.currentState = npc.meleeAttackState;
	}
	public void toRangeAttackState (){}

	void KeepWalking(){
            //Debug.Log("Walking");
		if(npc.myNavMeshAgent.enabled){
			npc.myNavMeshAgent.Resume();
			npc.npcMaster.callEventNpcWalkAnim();
	}
}
	void StopWalking(){
            //Debug.Log("Stopping");
            if (npc.myNavMeshAgent.enabled) {
			npc.myNavMeshAgent.Stop ();
			npc.npcMaster.callEventNpcIdleAnim();
		}
	}

	bool IsTargetInSight(){
		RaycastHit hit;
		Vector3 weaponLookAtVector = new Vector3 (npc.pursueTarget.position.x, npc.pursueTarget.position.y + npc.offset, npc.pursueTarget.position.z);
		npc.rangeWeapon.transform.LookAt (weaponLookAtVector);

		if (Physics.Raycast (npc.rangeWeapon.transform.position, npc.rangeWeapon.transform.forward, out hit)) {
			foreach (string tag in npc.myEnemyTags) {
				if (hit.transform.root.CompareTag (tag)) {
					return true;
				}
			}
			return false;
		} else {
			return false;
		}

	}

	void Look(){
		if (npc.pursueTarget == null) {
			toPatrolState ();
			return;
		}
		Collider[] colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange, npc.myEnemyLayers);
		if (colliders.Length == 0) {
			toPatrolState ();
			return;
		}
		foreach (Collider col in colliders) {
			if (col.transform.root == npc.pursueTarget) {
					TryToAttack ();
				return;
			}
		}
		toPatrolState ();
	}

	void TryToAttack(){
		if (npc.pursueTarget != null) {
			npc.meshRendererFlag.material.color = Color.cyan;
			if (!IsTargetInSight ()) {
				toPursueState ();
				return;
			}
			if (Time.time > npc.nextAttack) {
				npc.nextAttack = npc.attackRate;
				float distanceToTarget = Vector3.Distance (npc.transform.position, npc.pursueTarget.position);
				Vector3 newPos = new Vector3 (npc.pursueTarget.position.x, npc.transform.position.y, npc.pursueTarget.position.z);
				npc.transform.LookAt (newPos);
				if (distanceToTarget <= npc.rangeAttackRange) {
                        //Debug.Log("Stopping");
					StopWalking ();
					if (npc.rangeWeapon.GetComponent<Gun_Master> () != null) {
						npc.rangeWeapon.GetComponent<Gun_Master> ().CallEventNpcInput (npc.rangeAttackSpread);
                            //Debug.Log("Enter");
						return;

					}
				}
				if (distanceToTarget <= npc.meleeAttackRange && npc.hasMeleeAttack) {
					toMeleeAttackState ();
				}
			}
		} else {
			toPatrolState ();
		}
	}
}
}
