﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is strucked
	 */
public class NPCState_Struck : NPCState_Interface{
	private readonly NPC_StatePattern npc; // Refer to NPC_StatePattern script
	private float informRate=0.5f; // the frequency of inform
	private float nextInform; // the time of next inform
	private Collider[] colliders; // an array of player allies
	private Collider[] friendlyColliders; // an array of NPC allies

	public NPCState_Struck(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}

	public void updateState(){
		informNearbyAlliesThatIHaveBeenHurt ();
	}
	public void toPatrolState(){}
	public void toAlertState (){
		npc.currentState = npc.alertState;
	}
	public void toPursueState(){}
	public void toMeleeAttackState (){}
	public void toRangeAttackState (){}

	void informNearbyAlliesThatIHaveBeenHurt (){
		if (Time.time > nextInform) {
			nextInform = Time.time + informRate;
		} else {
			return;
		}

		if (npc.myAttacker != null) {
			friendlyColliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange, npc.myFriendlyLayers);

			if (IsAttackerClose ()) {
				AlertNearByAllies ();
				SetMySelfToInvestigate ();
			}
		}
	}

	bool IsAttackerClose(){
		if (Vector3.Distance (npc.transform.position, npc.myAttacker.position) <= npc.sightRange * 2) {
			return true;
		} else {
			return false;
		}
	}

	void AlertNearByAllies(){
		foreach (Collider ally in friendlyColliders) {
			if (ally.transform.root.GetComponent<NPC_StatePattern> () != null) {
				NPC_StatePattern allyPattern = ally.transform.root.GetComponent<NPC_StatePattern> ();

				if(allyPattern.currentState==allyPattern.patrolState){
					allyPattern.pursueTarget = npc.myAttacker;
					allyPattern.currentState = allyPattern.investigationHarmState;
					allyPattern.npcMaster.callEventNpcWalkAnim ();
			}

		}
	}
}

	void SetMySelfToInvestigate(){
		npc.pursueTarget = npc.myAttacker;
		npc.locationOfInterest = npc.myAttacker.position;

		if (npc.captureState == npc.patrolState) {
			npc.captureState = npc.investigationHarmState;
		}
	}

}
}
