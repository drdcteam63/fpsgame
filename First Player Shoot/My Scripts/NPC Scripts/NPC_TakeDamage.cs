﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DRDC1{
	/*
	 * implement the state when NPC is taking damage
	 */
public class NPC_TakeDamage : MonoBehaviour {

	private NPC_Master npcMaster; //Refer to the npcMaster script
	public int damageMultiplier = 1; // damage rate
	public bool shouldRemoveCollider; // Boolean: if should remove the Collider

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDie += RemoveThis;
	}

	void OnDisable(){
		npcMaster.EventNpcDie -= RemoveThis;
	}

	void SetInitialReferences(){
		npcMaster = transform.root.GetComponent<NPC_Master> ();
	}

	public void ProcessDamage(int damage){
            //Debug.Log("ProcessDamage");
		int damageToApply = damage * damageMultiplier;
		npcMaster.callEventNpcDeductHealth (damageToApply);
	}

	void RemoveThis(){
		if (shouldRemoveCollider) {
			if (GetComponent<Collider> () != null) {
				Destroy(GetComponent<Collider>());
			}
			if (GetComponent<Rigidbody> () != null) {
				Destroy (GetComponent<Rigidbody> ());
			}
		}

		gameObject.layer = LayerMask.NameToLayer ("Default");

		Destroy (this);
	}
}
}
