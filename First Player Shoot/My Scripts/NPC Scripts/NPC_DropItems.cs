﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC droping items
	 */
public class NPC_DropItems : MonoBehaviour {

	private NPC_Master npcMaster; //refer to NPC_Master script
	public GameObject[] itemsToDrop; // an array of dropped items
		void Start()
		{
			SetInitialReferences ();
		}
	void OnEnable(){
            SetInitialReferences();

        npcMaster.EventNpcDie += DropItems;
	}

	void OnDisable(){
		npcMaster.EventNpcDie -= DropItems;
	}

	void SetInitialReferences(){
		npcMaster = GetComponent<NPC_Master> ();

	}

	void DropItems(){
		if (itemsToDrop.Length > 0) {
			foreach (GameObject item in itemsToDrop) {
				StartCoroutine (PauseBeforeDrop (item));
			}
		}
	}

	IEnumerator PauseBeforeDrop(GameObject itemToDrop){
		yield return new WaitForSeconds (0.05f);
		itemToDrop.SetActive (true);
		itemToDrop.transform.parent = null;
		yield return new WaitForSeconds (0.05f);
		if(itemToDrop.GetComponent<Item_Master>()!=null)
			{
			itemToDrop.GetComponent<Item_Master>().CallEventObjectThrow();
		}
	}

}
}
