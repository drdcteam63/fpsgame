﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement NPC's Collision filed
	 */
public class NPC_CollisionField : MonoBehaviour {
	private NPC_Master npcMaster; // Refer to NPC_Master script
	private Rigidbody rigidBodyStrikingMe;
	private int damageToApply; // damage rate
	public float massRequirement = 50; //mass
	public float speedRequirement = 5; // speed
	private float damageFactor = 0.1f; //damage factor

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDie += DisableThis;
	}

	void OnDisable(){
		npcMaster.EventNpcDie -= DisableThis;
	}

	void OnTriggerEnter(Collider other){
		if (other.GetComponent<Rigidbody> () != null) {
			rigidBodyStrikingMe = other.GetComponent<Rigidbody> ();

			if (rigidBodyStrikingMe.mass >= massRequirement && rigidBodyStrikingMe.velocity.sqrMagnitude >= speedRequirement * speedRequirement) {
				damageToApply = (int)(rigidBodyStrikingMe.mass * rigidBodyStrikingMe.velocity.magnitude * damageFactor);
				npcMaster.callEventNpcDeductHealth (damageToApply);
			}
		}
	}

	void SetInitialReferences(){
		npcMaster = transform.root.GetComponent<NPC_Master> ();
	}

	void DisableThis(){
		gameObject.SetActive (false);
	}
}
}
