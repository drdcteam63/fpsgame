﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DRDC1{
	/*
	 * implement NPC holding weapon
	 */
public class NPC_HoldRangeWeapon : MonoBehaviour {
	private NPC_StatePattern npcStatePattern; // refer to NPC_StatePattern script
	private Animator myAnimator; // NPC's Animator
	public Transform rightHandTarget; //NPC's rightHand weapon
	public Transform leftHandTarget; //NPC's leftHand weapon

	// Use this for initialization
	void Start () {
			SetInitialReferences ();
	}

	void SetInitialReferences(){
		npcStatePattern = GetComponent<NPC_StatePattern> ();
		myAnimator = GetComponent<Animator> ();

	}

		void OnAnimatorIK(){
			if (npcStatePattern.rangeWeapon == null) {
				return;
			}

			if (myAnimator.enabled) {
				if (npcStatePattern.rangeWeapon.activeSelf) {
					if (rightHandTarget != null) {
						myAnimator.SetIKPositionWeight (AvatarIKGoal.RightHand, 1);
						myAnimator.SetIKRotationWeight (AvatarIKGoal.RightHand, 1);
						myAnimator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);
						myAnimator.SetIKRotation (AvatarIKGoal.RightHand, rightHandTarget .rotation);

					}
				}
			}
		}
}
}
