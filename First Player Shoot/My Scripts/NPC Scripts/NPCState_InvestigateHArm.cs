﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is investigating harm
	 */
public class NPCState_InvestigateHArm : NPCState_Interface {
	private readonly NPC_StatePattern npc; // Refer to NPC_StatePattern script
	private float offset = 0.3f;  // the offset on y direction when checking if player is in sight
	private RaycastHit hit;
	private Vector3 lookAtTarget; //the sight range of the enemy when looking at the target

	public NPCState_InvestigateHArm(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}

// Update once per frame
	public void updateState(){
		Look ();
	}
	// Change to patrol State
	public void toPatrolState(){
		npc.currentState = npc.patrolState;
	}
	// Change to alert state
	public void toAlertState (){
		npc.currentState = npc.alertState;
	}
	// Chage to pursue state
	public void toPursueState(){
		npc.currentState = npc.pursueState;
	}
	public void toMeleeAttackState (){}
	public void toRangeAttackState (){}

 //Check if the player is insight
	void Look(){
		if (npc.pursueTarget == null) {
			toPatrolState ();
			return;
		}

		CheckIfTargetIsInDirectSight ();
	}

	void CheckIfTargetIsInDirectSight(){
		lookAtTarget = new Vector3 (npc.pursueTarget.position.x, npc.pursueTarget.position.y + offset, npc.pursueTarget.position.z);

		if (Physics.Linecast (npc.head.position, lookAtTarget, out hit, npc.sightLayers)) {
			if (hit.transform.root == npc.pursueTarget) {
				npc.locationOfInterest = npc.pursueTarget.position;
				GoToLocationOfInterest ();

				if (Vector3.Distance (npc.transform.position, lookAtTarget) <= npc.sightRange) {
					toPursueState ();
				}
			} else {
				toAlertState ();
			}
		} else {
			toAlertState ();
		}
	}

 //Go to direction of interest
	void GoToLocationOfInterest(){
		npc.meshRendererFlag.material.color = Color.black;

		if (npc.myNavMeshAgent.enabled && npc.locationOfInterest != Vector3.zero) {
			npc.myNavMeshAgent.SetDestination (npc.locationOfInterest);
			npc.myNavMeshAgent.Resume ();
			npc.npcMaster.callEventNpcWalkAnim ();

			if (npc.myNavMeshAgent.remainingDistance <= npc.myNavMeshAgent.stoppingDistance) {
				npc.locationOfInterest = Vector3.zero;
				toPatrolState ();
			}
		 else {
			toPatrolState ();
		}
	}
}
}
}
