﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement turning off Animator
	 */
public class NPC_TurnOffAnimator : MonoBehaviour {

	private NPC_Master npcMaster;  //Refer to the npcMaster script
	private Animator myAnimator;  //Refer to the Animator of the NPC

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDie += TurnOffAnimator;

	}

	void OnDisable(){
		npcMaster.EventNpcDie -= TurnOffAnimator;

	}

	void SetInitialReferences(){
		npcMaster = GetComponent<NPC_Master> ();

		if (GetComponent<Animator> () != null) {
			myAnimator = GetComponent<Animator> ();
		}
	}

	void TurnOffAnimator(){
		if (myAnimator != null) {
			myAnimator.enabled = false;
		}
	}
}
}
