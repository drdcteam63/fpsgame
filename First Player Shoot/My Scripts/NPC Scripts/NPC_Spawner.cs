﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1
{
  /*
	 * spawn bomb and enemy
	 */
    public class NPC_Spawner : MonoBehaviour
    {
		private GoalSetting goalsetting; // refer to goalsetting script
		private DataReceiver datareceiver; //refer to datareceiver script
		private int enemyNumber=0; //the number of enemy that player wish to generate
		private int bombNumber=0; // the nunber of bomb the player wish to generate
        private float spawnRadius = 100; // the radius of the generated enemy
		private float bombspawnRadius = 10; // the radius of the generated bomb
        public GameObject objectToSpawn; // the object to spawn (bomb or enemy)
        public GameObject objectToSpawn2; // same, in scene2
        public GameObject map; // the terrain object
        private Vector3 spawnPosition; // the position to spawn the enemy
        private Vector3 spawnPosition2; //same, in scene2s
        private Vector3 mapCentrePosition; // the location of the map centre
        private Vector3 BombCenterPosition; // the locaiton of the bomb


		void Awake(){

		}
        void Start()
        {
            SetInitialReference();
            //SpawnEnemy();
        }

		void Update(){
			if (map.CompareTag ("Level2Scene")) {
				if ((enemyNumber == 0) && (bombNumber == 0)) {
					enemyNumber = datareceiver.EnemyNumberGetter ();
					bombNumber = datareceiver.BombaNumberGetter ();
					SpawnEnemy ();
				}
			}
            else
            {
                if ((enemyNumber == 0) && (bombNumber == 0))
                {

                    enemyNumber = goalsetting.EnemyNumberGetter();
                    bombNumber = goalsetting.BombaNumberGetter();
                    SpawnEnemy();
                }
                //Debug.LogWarning ("Bomba" + bombNumber);
                //Debug.LogWarning ("Enemy" + enemyNumber);
            }
        }
        public void getMapCentrePosition()
		{

			if (map.CompareTag ("Level2Scene")) {
				mapCentrePosition.x = map.GetComponent<Terrain> ().terrainData.size.x / 2 + map.transform.position.x-5;
				mapCentrePosition.z = map.GetComponent<Terrain> ().terrainData.size.z / 2 + map.transform.position.z-150;
                spawnRadius = 3;
                bombspawnRadius = 5;

            } else{
				mapCentrePosition.x = map.GetComponent<Terrain> ().terrainData.size.x / 2 + map.transform.position.x;
				mapCentrePosition.z = map.GetComponent<Terrain> ().terrainData.size.z / 2 + map.transform.position.z;
			}

			if (map.CompareTag ("Level2Scene")) {
				BombCenterPosition.x = map.GetComponent<Terrain> ().terrainData.size.x / 2 + map.transform.position.x - 53; //scene2
				BombCenterPosition.z = map.GetComponent<Terrain> ().terrainData.size.z / 2 + map.transform.position.z - 163;//scene2
			} else {
				BombCenterPosition.x= map.GetComponent<Terrain>().terrainData.size.x / 2 + map.transform.position.x-150;//scene1
				BombCenterPosition.z = map.GetComponent<Terrain>().terrainData.size.x / 2 + map.transform.position.x - 150;//scene1
			}
		}

        public void SpawnEnemy()
        {
            getMapCentrePosition();

            for (int i = 0; i < enemyNumber;)
            {
				//Debug.Log ("Enemy"+i+"Spawned");
                spawnPosition = mapCentrePosition + Random.insideUnitSphere * spawnRadius;
                if (Terrain.activeTerrain.SampleHeight(spawnPosition) < 1)
                {
                    spawnPosition.y = 0.1f;
                    Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);
                    //Debug.Log("Enemy i position:" + spawnPosition);
                    i++;
                }

            }

            for (int j = 0; j < bombNumber;)
            {
				//Debug.Log ("Bomba"+j+"Spawned");
                spawnPosition2 = BombCenterPosition + Random.insideUnitSphere * bombspawnRadius;
                if (Terrain.activeTerrain.SampleHeight(spawnPosition2) < 1)
                {
                    spawnPosition2.y = 0.1f;
                    Instantiate(objectToSpawn2, spawnPosition2, Quaternion.identity);
                    //Debug.Log("bomb j position:" + spawnPosition2);
                    j++;
                }
            }
        }

		void SetInitialReference(){
			if (map.CompareTag ("Level2Scene")) {
				datareceiver = this.GetComponent<DataReceiver>();
			} else {
				goalsetting=this.GetComponent<GoalSetting>();
			}
		}
    }
}
