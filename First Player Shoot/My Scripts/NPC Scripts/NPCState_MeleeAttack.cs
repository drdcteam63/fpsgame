﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is doing melee attack
	 */
public class NPCState_MeleeAttack : NPCState_Interface{
	private readonly NPC_StatePattern npc; // refer to NPC_StatePattern script
	private float distanceToTarget; //the distance from enemy to Player

	public NPCState_MeleeAttack(NPC_StatePattern NPC_StatePattern){
		npc = NPC_StatePattern;
	}

// Update once per frame
	public void updateState(){
		Look ();
		TryToAttack ();
	}

 // Change to patrol state
	public void toPatrolState(){
		KeepWalking ();
			npc.pursueTarget = null;
			npc.isMeleeAttacking = false;
		npc.currentState = npc.patrolState;
	}

 // Change to alert state
	public void toAlertState (){
		KeepWalking ();
		npc.currentState = npc.alertState;
	}

 //Change to pursue state
	public void toPursueState(){
		KeepWalking ();
			npc.isMeleeAttacking = false;
		npc.currentState = npc.pursueState;
	}

	public void toMeleeAttackState (){}
	public void toRangeAttackState (){}

 // keep finding the player when Player is not in the distance
	void Look(){
		if (npc.pursueTarget == null) {
			toPatrolState ();
			return;
		}

		Collider[] colliders = Physics.OverlapSphere (npc.transform.position, npc.meleeAttackRange, npc.myEnemyLayers);

		if (colliders.Length == 0) {
			//npc.pursueTarget = null;
			toPatrolState ();
			//toPursueState();
			return;
		}

		foreach (Collider col in colliders) {
			if (col.transform.root == npc.pursueTarget) {
				return;
			}
		}

		//npc.pursueTarget = null;
		toPatrolState ();
		//toPursueState();
	}

 // Try to attack the player when player is within the melee attack distance
	void TryToAttack(){
		if (npc.pursueTarget != null) {
			npc.meshRendererFlag.material.color = Color.magenta;

			if (Time.time > npc.nextAttack && !npc.isMeleeAttacking) {
				npc.nextAttack = Time.time + npc.attackRate;
				if (Vector3.Distance (npc.transform.position, npc.pursueTarget.position) <= npc.meleeAttackRange) {
						Vector3 newPos = new Vector3 (npc.pursueTarget.position.x, npc.pursueTarget.position.y, npc.pursueTarget.position.z);
					npc.transform.LookAt (newPos);
					npc.npcMaster.callEventNpcAttackAnim ();
                    npc.OnEnemyAttack();
                    npc.isMeleeAttacking = true;
				}
			}
                else
                {

                    toPursueState();
                }
            } else {
                toPatrolState ();
		}
	}

 // Keep walk when not try to attack
	void KeepWalking(){
		npc.myNavMeshAgent.Resume ();
		npc.npcMaster.callEventNpcWalkAnim ();
	}
}
}
