﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * Organize player's health information
	 */
public class NPC_Health : MonoBehaviour {
	private NPC_Master npcMaster; // refer to NPC_Master script
	public int npcHealth=100;
	private bool healthCritical; // if health is low
	private float healthLow = 25;

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDeductHealth += DeductHealth;
		npcMaster.EventNpcIncreaseHealth += IncreaseHealth;
	}

	void OnDisble(){
		npcMaster.EventNpcDeductHealth -= DeductHealth;
		npcMaster.EventNpcIncreaseHealth -= IncreaseHealth;
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.Period)) {
			npcMaster.callEventNpcIncreaseHealth (20);
		}
	}

	void SetInitialReferences(){
		npcMaster = GetComponent<NPC_Master> ();
	}

	void DeductHealth(int healthChange){
		npcHealth -= healthChange;
		if (npcHealth <= 0) {
			npcHealth = 0;
			npcMaster.callEventNpcDie ();
			Destroy (gameObject, Random.Range (10, 20));
		}
		CheckHealthFraction ();
	}

	void IncreaseHealth(int healthChange){
		npcHealth += healthChange;
		if (npcHealth > 100) {
			npcHealth = 100;
		}
		CheckHealthFraction ();
	}


	void CheckHealthFraction(){
		if (npcHealth <= healthLow && npcHealth > 0) {
			npcMaster.callEventNpcLowHealth ();
			healthCritical = true;
		} else if (npcHealth > healthLow && healthCritical) {
			npcMaster.callEventNpcHealthRecovered ();
				healthCritical=false;
		}

	}
}
}
