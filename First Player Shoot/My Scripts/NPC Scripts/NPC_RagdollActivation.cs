﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
public class NPC_RagdollActivation : MonoBehaviour {
	/*
	 * implement ragdoll effect
	 */
	private NPC_Master npcMaster; // refer to NPC_Master script
	private Rigidbody myRigidbody; // refer to NPC's Rigidbody
	private Collider myCollider; //NPC's Collider

	void OnEnable(){
		SetInitialReferences ();
		npcMaster.EventNpcDie += ActivateRagdoll;
	}

	void OnDisable(){
		npcMaster.EventNpcDie -= ActivateRagdoll;
	}

	void SetInitialReferences(){
		npcMaster = transform.root.GetComponent<NPC_Master> ();

		if (GetComponent<Collider> () != null) {
			myCollider = GetComponent<Collider> ();
		}

		if (GetComponent<Rigidbody> () != null) {
			myRigidbody = GetComponent<Rigidbody> ();
		}
	}

	void ActivateRagdoll(){
		if (myCollider != null) {
			myCollider.enabled = true;
			myCollider.isTrigger = false;
		}

		if (myRigidbody != null) {
			myRigidbody.isKinematic = false;
			myRigidbody.useGravity = true;
		}

		gameObject.layer = LayerMask.NameToLayer ("Default");
	}

}
}
