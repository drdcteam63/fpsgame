﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is pursuing the player
	 */
public class NPCState_Pursue : NPCState_Interface{
	private readonly NPC_StatePattern npc; // the refer to NPC_StatePattern script
	private float captureDistance; // Calculate the distance of the player from the enemy

	public NPCState_Pursue(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}

 // Update is called once per frame
	public void updateState(){
		Look ();
		Pursue ();
	}

	//change to patrol state
	public void toPatrolState(){
		KeepWalking ();
			npc.pursueTarget = null;
		npc.currentState = npc.patrolState;
	}

// change to alert state
	public void toAlertState (){
            KeepWalking ();
		npc.currentState = npc.alertState;
	}
	public void toPursueState(){}

 // change to melee attack state
	public void toMeleeAttackState (){
		npc.currentState = npc.meleeAttackState;
	}
	// Change to range attack state
	public void toRangeAttackState (){
            npc.currentState = npc.rangeAttackState;
	}

// Keep finding the player when player is not in the distance
	void Look(){
		if (npc.pursueTarget == null) {
			toPatrolState ();
			return;
		}

		Collider[] colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange, npc.myEnemyLayers);

		if (colliders.Length == 0) {
			npc.pursueTarget=null;
			toPatrolState ();
			return;
		}
		captureDistance = npc.sightRange * 2;
		foreach (Collider col in colliders) {
			float distanceToTarg = Vector3.Distance (npc.transform.position, col.transform.position);

			if (distanceToTarg < captureDistance) {
				captureDistance = distanceToTarg;
				npc.pursueTarget = col.transform;
			}
		}
	}

//when player is within pursue distance, enter pursue state
	void Pursue(){
		npc.meshRendererFlag.material.color = Color.red;
            //Debug.Log(npc.pursueTarget);
		if (npc.myNavMeshAgent.enabled && npc.pursueTarget != null) {
			npc.myNavMeshAgent.SetDestination (npc.pursueTarget.position);
			npc.locationOfInterest = npc.pursueTarget.position;//used by alert state
                KeepWalking ();

			float distanceToTarget = Vector3.Distance (npc.transform.position, npc.pursueTarget.position);

			if (distanceToTarget <= npc.rangeAttackRange && distanceToTarget > npc.meleeAttackRange) {
				if (npc.hasRangeAttack) {
					toRangeAttackState ();
				}
			} else if (distanceToTarget <= npc.meleeAttackRange) {
				if (npc.hasMeleeAttack) {
					toMeleeAttackState ();
				} else if (npc.hasRangeAttack) {
					toRangeAttackState ();
				}
			}
		} else {
			toAlertState ();
		}
	}

//Keep walking when player is not in the distance
	void KeepWalking(){
		npc.myNavMeshAgent.Resume ();
		npc.npcMaster.callEventNpcWalkAnim ();
	}
}
}
