﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is alert
	 */
public class NPCState_Alert : NPCState_Interface{
	private readonly NPC_StatePattern npc; // Refer to NPC_StatePattern script
	private float informRate=3; //Rate of inform
	private float nextInform; // time of next inform
	private float offset=0.3f;
	private Vector3 targetPosition; // the positon of the player
	private RaycastHit hit;
	private Collider[] colliders;
	private Collider[] friendlyColliders;
	private Vector3 lookAtTarget;
	private int detectionCount=0; //store the number of allies detected
	private int lastDetectionCount;
	private Transform possibleTarget;

	public NPCState_Alert(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}
//Uodate once per frame
	public void updateState(){
		Look ();
		GoToLocationOfInterest ();
	}
	//change to patrol state
	public void toPatrolState(){
		npc.currentState = npc.patrolState;
	}
	public void toAlertState (){}
	// Chage to pursue state
	public void toPursueState(){
		npc.currentState = npc.pursueState;
	}
	public void toMeleeAttackState (){}
	public void toRangeAttackState (){}

	void Look(){
		colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange, npc.myEnemyLayers);
		lastDetectionCount = detectionCount;
		foreach (Collider col in colliders){
			lookAtTarget = new Vector3 (col.transform.position.x, col.transform.position.y + offset, col.transform.position.z);

			if (Physics.Linecast (npc.head.position, lookAtTarget, out hit, npc.sightLayers)) {
				foreach (string tags in npc.myEnemyTags) {
					if (hit.transform.CompareTag (tags)) {
						detectionCount++;
							possibleTarget = col.transform.root;
						break;
					}
				}
			}
		}
		//Check if detection count has changed and if not then set it back to 0
		if(detectionCount==lastDetectionCount){
			detectionCount = 0;
	}
		//Check if detection count is greater than the requirement and if so pursue
			if (detectionCount >= npc.requiredDetectionCount) {
				detectionCount = 0;
				npc.locationOfInterest = possibleTarget.position;
				npc.pursueTarget = possibleTarget.root;
				InformNearbyAllies ();
				toPursueState ();
			}
}
	void GoToLocationOfInterest(){
		npc.meshRendererFlag.material.color = Color.yellow;
		if (npc.myNavMeshAgent.enabled && npc.locationOfInterest != Vector3.zero) {
			npc.myNavMeshAgent.SetDestination (npc.locationOfInterest);
			npc.myNavMeshAgent.Resume ();
			npc.npcMaster.callEventNpcWalkAnim ();

			if (npc.myNavMeshAgent.remainingDistance <= npc.myNavMeshAgent.stoppingDistance && !npc.myNavMeshAgent.pathPending) {
				npc.npcMaster.callEventNpcIdleAnim ();
				npc.locationOfInterest = Vector3.zero;
				toPatrolState ();
			}
		}
	}

	void InformNearbyAllies(){
		if (Time.time > nextInform) {
			nextInform = Time.time + informRate;
			friendlyColliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange, npc.myFriendlyLayers);

			if(friendlyColliders.Length==0){
				return;
		}
			foreach (Collider ally in friendlyColliders) {
				if (ally.transform.root.GetComponent<NPC_StatePattern> () != null) {
					NPC_StatePattern allyPattern = ally.transform.root.GetComponent<NPC_StatePattern> ();

					if (allyPattern.currentState == allyPattern.patrolState) {
						allyPattern.pursueTarget = npc.pursueTarget;
						allyPattern.locationOfInterest = npc.pursueTarget.position;
						allyPattern.currentState = allyPattern.alertState;
						allyPattern.npcMaster.callEventNpcWalkAnim ();
					}
				}
			}
	}
  }
}
}
