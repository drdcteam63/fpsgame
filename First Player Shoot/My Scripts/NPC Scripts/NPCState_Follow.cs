﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement the state when NPC is following the player
	 */
public class NPCState_Follow : NPCState_Interface {
	private readonly NPC_StatePattern npc; // Refer to NPC_StatePattern script
	private Collider[] colliders; // An array of colliders
	private Vector3 lookAtPoint; // Store the location of the target
	private Vector3 heading; // Store the heading location
	private float dotProd;

	public NPCState_Follow(NPC_StatePattern npcStatePattern){
		npc = npcStatePattern;
	}

// Update once per frame
	public void updateState(){
		Look ();
		FollowTarget ();
	}
	// Change to patrol state
	public void toPatrolState(){
		npc.currentState = npc.patrolState;
	}
	// Change to alert state
	public void toAlertState (){
		npc.currentState = npc.alertState;
	}
	public void toPursueState(){}
	public void toMeleeAttackState (){}
	public void toRangeAttackState (){}

 // Find the player
	void Look(){
		//check near range
		colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange / 3, npc.myEnemyLayers);

		if (colliders.Length > 0) {
			AlertStateActions (colliders [0].transform);
			return;
		}
		//check medium range
		colliders = Physics.OverlapSphere (npc.transform.position, npc.sightRange / 2, npc.myEnemyLayers);

		if (colliders.Length > 0) {
			VisibilityCalculations (colliders [0].transform);
			if (dotProd > 0) {
				AlertStateActions (colliders [0].transform);
				return;
			}
			return;
		}
	//check max Range
		colliders= Physics.OverlapSphere(npc.transform.position, npc.sightRange, npc.myEnemyLayers);
	foreach(Collider col in colliders){
		RaycastHit hit;
		VisibilityCalculations(col.transform);
			if(Physics.Linecast(npc.head.position, lookAtPoint, out hit, npc.sightLayers)){
				foreach(string  tags in npc.myEnemyTags){
					if(hit.transform.CompareTag(tags)){
						if(dotProd>0){

							AlertStateActions(col.transform);
							return;
					}
				}
			}
	}
				}
	}

 // Start follow the player
	void FollowTarget(){
		npc.meshRendererFlag.material.color = Color.blue;
		if (!npc.myNavMeshAgent.enabled) {
			return;
		}
		if (npc.myFollowTarget != null) {
			npc.myNavMeshAgent.SetDestination (npc.myFollowTarget.position);
			KeepWalking ();
		} else {
			toPatrolState ();
		}
		if (HaveIReachedDestination ()) {
			StopWalking ();
		}
	}





//Chage to alert state
	void AlertStateActions(Transform target){
		npc.locationOfInterest = target.position;//For check state
		toAlertState();
	}
	//Calculate if the player is in sight
	void VisibilityCalculations(Transform target){
		lookAtPoint = new Vector3 (target.position.x, target.position.y + npc.offset, target.position.z);
		heading = lookAtPoint - npc.transform.position;
		dotProd = Vector3.Dot (heading, npc.transform.forward);
	}

 //Calculate if reached the destination
	bool HaveIReachedDestination(){
		if (npc.myNavMeshAgent.remainingDistance <= npc.myNavMeshAgent.stoppingDistance && !npc.myNavMeshAgent.pathPending) {
			StopWalking ();
			return true;
		} else {
			KeepWalking ();
			return false;
		}
	}

	void KeepWalking(){
		npc.myNavMeshAgent.Resume ();
		npc.npcMaster.callEventNpcWalkAnim ();
	}

	void StopWalking(){
		npc.myNavMeshAgent.Stop ();
		npc.npcMaster.callEventNpcIdleAnim ();
	}
}
}
