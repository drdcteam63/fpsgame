﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace DRDC1{
	/*
	 * organize all the possible state of the NPC
	 */
public class NPC_StatePattern : MonoBehaviour {
	private float checkRate = 0.1f; //Check frequency
	private float nextCheck; // the time of the next check
	public float sightRange=40; // the sightRange of the NPC
	public float detectBehindRange=5; //the detect range outside the sightRange
	public float meleeAttackRange = 7; // the range for meleeAttack
	public float meleeAttackDamage = 10; //the damage rate for melee attack
	public float rangeAttackRange = 35; //the range for rangeattack
	public float rangeAttackDamage=5; //the damage for range attack
	public float rangeAttackSpread = 0.5f; //the spread for range attack
	public float attackRate=0.4f; //the frequency of attack
	public float nextAttack; // the time for next attack
	public float fleeRange = 25; //the range for flee
	public float offset = 0.4f;
	public int requiredDetectionCount = 15;

	public bool hasRangeAttack; // whether had range attack state
	public bool hasMeleeAttack; // whether had in melee attack state
	public bool isMeleeAttacking; // if is in meleeAttack state

	public Transform myFollowTarget;
	[HideInInspector]
	public Transform pursueTarget;
	[HideInInspector]
	public Vector3 locationOfInterest;
	[HideInInspector]
	public Vector3 wanderTarget;
	[HideInInspector]
	public Transform myAttacker;

	public LayerMask sightLayers;
	public LayerMask myFriendlyLayers;
	public LayerMask myEnemyLayers;
	public string[] myEnemyTags;
	public string[] myFriendlyTags;

	public Transform[] wayPoints;
	public Transform head;
	public MeshRenderer meshRendererFlag;
	public GameObject rangeWeapon;
	public NPC_Master npcMaster;
	[HideInInspector]
	public NavMeshAgent myNavMeshAgent;

	public NPCState_Interface currentState;
	public NPCState_Interface captureState;
	public NPCState_Patrol patrolState;
	public NPCState_Alert alertState;
	public NPCState_Pursue pursueState;
	public NPCState_MeleeAttack meleeAttackState;
	public NPCState_RangeAttack rangeAttackState;
	public NPCState_Flee FleeState;
	public NPCState_Struck struckState;
	public NPCState_InvestigateHArm investigationHarmState;
	public NPCState_Follow followState;

	void Awake(){
		SetUpStateReferences ();
		SetInitialReferences ();
		npcMaster.EventNpcLowHealth += ActivateFleeState;
		npcMaster.EventNpcHealthRecovered += ActivatePatrolState;
		npcMaster.EventNpcDeductHealth += ActivateStruckState;
	}

	void Start(){
			SetUpStateReferences ();
	}

	void OnDisable(){
		npcMaster.EventNpcLowHealth -= ActivateFleeState;
		npcMaster.EventNpcHealthRecovered -= ActivatePatrolState;
		npcMaster.EventNpcDeductHealth -= ActivateStruckState;
		StopAllCoroutines ();
	}

	void Update(){
		CarryOutUpdateState ();
	}

	void SetUpStateReferences(){
		patrolState = new NPCState_Patrol (this);
		alertState = new NPCState_Alert (this);
		pursueState = new NPCState_Pursue (this);
		FleeState = new NPCState_Flee (this);
		followState = new NPCState_Follow (this);
		rangeAttackState= new NPCState_RangeAttack (this);
		struckState = new NPCState_Struck (this);
		investigationHarmState = new NPCState_InvestigateHArm (this);
		meleeAttackState = new NPCState_MeleeAttack (this);
	}

	void SetInitialReferences(){
			npcMaster = GetComponent<NPC_Master> ();
            if(GetComponent<NavMeshAgent>()!=null)
            {
                myNavMeshAgent = GetComponent<NavMeshAgent>();
            }
            ActivatePatrolState();

    }

	void CarryOutUpdateState(){
		if (Time.time > nextCheck) {
			nextCheck = Time.time + checkRate;
			currentState.updateState ();
				//Debug.Log (currentState);
		}
	}

	void ActivatePatrolState(){
		currentState = patrolState;
	}

	void ActivateFleeState(){
		if (currentState == struckState) {
			captureState = FleeState;
			return;
		}
		currentState = FleeState;
	}

	void ActivateStruckState(int dummy){
		StopAllCoroutines ();
		if (currentState != struckState) {
			captureState = currentState;
		}

			if (rangeWeapon != null) {
				rangeWeapon.SetActive (false);
			}

			if(myNavMeshAgent.enabled){
				myNavMeshAgent.Stop ();
		}
			currentState = struckState;
			isMeleeAttacking = false;
		npcMaster.StartCoroutine (RecoverFromStruckState());
	  }

		IEnumerator RecoverFromStruckState(){
		yield return new WaitForSeconds (1.5f);
		if (rangeWeapon != null) {
				rangeWeapon.SetActive(true);
		}
		if (myNavMeshAgent.enabled) {
			myNavMeshAgent.Resume();
		}
			currentState=captureState;
	  }

	public void OnEnemyAttack(){
		if (pursueTarget != null) {
			if (Vector3.Distance (transform.position, pursueTarget.position) <= meleeAttackRange) {
				Vector3 toOther = pursueTarget.position - transform.position;
				if (Vector3.Dot (toOther, transform.forward) > 0.5f) {
						pursueTarget.SendMessage("CallEventPlayerHealthDeduction", meleeAttackDamage, SendMessageOptions.DontRequireReceiver);
						pursueTarget.SendMessage("ProcessDamage", meleeAttackDamage, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		                isMeleeAttacking = false;
	}

	public void SetMyAttacker(Transform attacker){
			myAttacker = attacker;
	}

	public void Distract(Vector3 DistractionPos){
			locationOfInterest=DistractionPos;
		if (currentState == patrolState) {
			currentState = alertState;
		}
	}


}
}
