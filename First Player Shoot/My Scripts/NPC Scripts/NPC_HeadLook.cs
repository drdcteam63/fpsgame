﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DRDC1{
	/*
	 * implement NPC's looking action
	 */
public class NPC_HeadLook : MonoBehaviour {
		private NPC_StatePattern npcStatePattern; // refer to NPC_StatePattern script
		private Animator myAnimator; //Refer to NPC Animator


	// Use this for initialization
	void Start () {
			SetInitialReferences ();
	}

		void SetInitialReferences(){
			npcStatePattern = GetComponent<NPC_StatePattern> ();
			myAnimator = GetComponent<Animator> ();
		}

		void OnAnimatorIK(){
			if(npcStatePattern.pursueState!=null){
				myAnimator.SetLookAtWeight (1, 0, 0.5f, 0.5f, 0.7f);
				myAnimator.SetLookAtPosition(npcStatePattern.pursueTarget.position);
		}

			else{
				myAnimator.SetLookAtWeight(0);
			}
}

}
}
