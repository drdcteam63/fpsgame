﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * implementing the function of tutorial at the beginning of the game
 */
namespace DRDC1
{
    public class TaskIntroduction : MonoBehaviour
    {
        public Sprite[] imagesources;//shore the materials of the tutorial
        public GameObject ButtonText;
        public GameObject DisplayImage;
        public GameObject Button;
        public GameObject Canvas;
        private bool toogle=false;
        private int index=0;
        private GameManager_Master gameManagerMaster;
        private GameManager_ToggleSetting tooglesetting;
        // Use this for initialization

        void SetInitialReferences()
        {
            gameManagerMaster = GetComponent<GameManager_Master>();
            tooglesetting = GetComponent<GameManager_ToggleSetting>();
        }
        void Start()
        {
            SetInitialReferences();
        }

        // Update is called once per frame
        void Update()
        {
            if(toogle==true)
            {
                DisplayImage.GetComponent<Image>().sprite = imagesources[index];
                if(index== imagesources.Length-1)
                {
                    ButtonText.GetComponent<Text>().text = "Finish";//changing the button's name from 'next' to 'finish' when tutorial is  over
                }
            }
        }

        public void opencanvas()
        {
            Canvas.SetActive(true);
            toogle = true;
        }

        public void closecanvas()
        {
            if(index== imagesources.Length - 1)
            {
                Canvas.SetActive(false);
                gameManagerMaster.CallEventMenuToggle();
                tooglesetting.ToggleOpenSettingUI();
            }
            
        }

        public void next()
        {
            if(index< imagesources.Length - 1)
            index++;
        }
    }
}

