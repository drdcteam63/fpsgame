﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/*
 * displaying information on map canvas
 */
namespace DRDC1
{
    public class MapTaskInfo : MonoBehaviour, IDragHandler, IPointerClickHandler
    {
        public GameObject TaskInformation;
        public void OnDrag(PointerEventData eventData)
        {
            throw new NotImplementedException();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            TaskInformation.SetActive(!TaskInformation.activeSelf);
        }


    }
}

