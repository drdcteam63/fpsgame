﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * implementing the function of judging whether goals have been finished in Game1 Scene
 */
namespace DRDC1
{
    public class GameManager_GameGoalsScene2 : MonoBehaviour
    {

        private int bombacount = 0;//the number of defusing BombA successfully
        private int bombbcount = 0;//the number of defusing BombB successfully
        private int bombafailcount = 0;// the number of defusing BombA unsucessfully
        private int bombbfailcount = 0;// the number of defusing BombB unsucessfully
        private int bombagoal = 1;// the number of BombAs in Goal-Setting Page
        private int bombbgoal = 4;// the number of BombBs in Goal-Setting Page
        private int heartrategoal;//the threshold of heartrate in Goal-Setting Page
        public GameObject Canvas;
        public GameObject FailCanvas;

        void Start()//initialize all paramaters
        {
            bombagoal = PlayerPrefs.GetInt("bombanumber");
            bombbgoal = PlayerPrefs.GetInt("bombnumber");
            bombacount = PlayerPrefs.GetInt("bombacount");
            bombbcount = PlayerPrefs.GetInt("bombbcount");
            bombafailcount = PlayerPrefs.GetInt("bombafailcount");
			heartrategoal = PlayerPrefs.GetInt ("HeartRateValue");
            
            Debug.Log(bombagoal);
            Debug.Log(bombbgoal);
            Debug.Log(bombacount);
            Debug.Log(bombafailcount);
            Debug.Log(bombbcount);
            Debug.Log(bombbfailcount);
            Debug.Log(PlayerPrefs.GetString("selftalk"));
        }
        // Update is called once per frame
        void Update()//checking goals are finished or not and giving different result based on different condition
        {
            if (bombacount == bombagoal && bombbcount == bombbgoal)
            {
                Canvas.SetActive(true);
            }
            else if((bombafailcount > 0 && bombacount + bombafailcount == bombagoal) && (bombbfailcount > 0 && bombbcount + bombbfailcount == bombbgoal))
            {
                FailCanvas.SetActive(true);
            }
            else if ((bombafailcount > 0 && bombacount + bombafailcount == bombagoal) && (bombbfailcount == 0 && bombbcount + bombbfailcount == bombbgoal))
            {
                FailCanvas.SetActive(true);
            }
        }

        public void bombAdefusedsuccess()//increase the number of defusing BombA successfully
        {
            bombacount++;
        }
        public void bombAdefusedfail()//increase the number of defusing BombA unsuccessfully
        {
            bombafailcount++;
        }
        public void bombBdefusedfail()//increase the number of defusing BombB unsuccessfully
        {
            bombbfailcount++;
            Debug.Log(bombbfailcount);
        }
        public void bombBdefusedsuccess()//increase the number of defusing BombB successfully
        {
            bombbcount++;
        }
        public void resetcount()//resetting paramaters
        {
            bombacount = 0;
            bombbcount = 0;
        }
        public void setbombA(int number)//setting bombagoal
        {
            bombagoal = number;
            Debug.Log(bombagoal);
        }
        public void setbombB(int number)//setting bombbgoal
        {
            bombbgoal = number;
        }

        public int bombacountGetter()//return bombacount
        {
            return bombacount;
        }

        public int bombbcountGetter()//return bombbcount
        {
            return bombbcount;
        }

        public int bombagoalGetter()//return bombagoal
        {
            return bombagoal;
        }

        public int bombbgoalGetter()//return bombbgoal
        {
            return bombbgoal;
        }

		public int heartrateGetter (){//return heartrategoal
			return heartrategoal;
		}
    }
}
