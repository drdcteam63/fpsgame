﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Implementing the function of controling the Instruction Panel
 */
namespace DRDC1
{
    public class GameManager_PanelInstructions : MonoBehaviour
    {
        public GameObject panelInstructions;
        private GameManager_Master gameManagerMaster;

        private void OnEnable()//Add TurnOffPanelInstructions to GameOverEvent 
        {
            SetInitialReferences();
            gameManagerMaster.GameOverEvent += TurnOffPanelInstructions;
        }

        private void OnDisable()//Delete TurnOffPanelInstructions from GameOverEvent
        {
            gameManagerMaster.GameOverEvent -= TurnOffPanelInstructions;
        }

        private void SetInitialReferences()//Initialization
        {
            gameManagerMaster = GetComponent<GameManager_Master>();
        }

        private void TurnOffPanelInstructions()//Turning off the Instruction Panel
        {
            if(panelInstructions!=null)
            {
                panelInstructions.SetActive(false);
            }
        }
    }
}


