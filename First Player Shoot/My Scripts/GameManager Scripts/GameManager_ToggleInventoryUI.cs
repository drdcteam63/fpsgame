﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Controlling the Inventory
 */
namespace DRDC1
{
    public class GameManager_ToggleInventoryUI : MonoBehaviour
    {
        [Tooltip("sjadlaksjdlk")]
        public bool hasInventory;
        public GameObject inventoryUI;
        public string toggleInventoryButton;
        private GameManager_Master gameManagerMaster;
        // Use this for initialization
        void Start()
        {
            SetInitialReferences();
        }

        // Update is called once per frame
        /*
        void Update()
        {
            CheckForInventoryUIToggleRequest();
        }
        */
        void SetInitialReferences()//initialization
        {
            gameManagerMaster = GetComponent<GameManager_Master>();

            if(toggleInventoryButton=="")
            {
                Debug.LogWarning("no name");
                this.enabled = false;
            }
        }

        void CheckForInventoryUIToggleRequest()//detecting if players send the operation to the inventory
        {
            
            if(Input.GetButtonUp(toggleInventoryButton)&&!gameManagerMaster.isMenuOn&&!gameManagerMaster.isGameOver&&hasInventory)
            {
                ToggleInventoryUI();
                Debug.LogWarning("detect input");
            }
            else
            {
                //Debug.LogWarning("asdasdasdasd12121212");
            }
        }

		public void ToggleInventoryUI()//open or close inventory
        {
            if(inventoryUI!=null)
            {
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                gameManagerMaster.isInventoryUIOn = !gameManagerMaster.isInventoryUIOn;
                gameManagerMaster.CallEventInventoryUIToggle();
            }
        }
    }
}


