﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * implementing the function of judging whether goals have been finished in Game Scene
 */

namespace DRDC1
{
    public class GameManager_GameGoals : MonoBehaviour
    {
        static private int bombacount = 0; //the number of defusing BombA successfully
        static private int bombbcount = 0;//the number of defusing BombB successfully
        private int bombafailcount = 0;// the number of defusing BombA unsucessfully
        private int bombbfailcount = 0;// the number of defusing BombB unsuccessfully
        private int bombagoal = 1;// the number of BombAs in Goal-Setting Page
        private int bombbgoal = 4;//the number of BombBs in Goal-Setting Page
        public GameObject Canvas;
        public GameObject FailCanvas;


        // Update is called once per frame, checking goals are finished or not
        void Update()
        {
            transmitdata();
            //Debug.Log(bombacount);
            if (bombacount == bombagoal && bombbcount == bombbgoal)
            {
                Canvas.SetActive(true);
            }
            /*
            if ((bombafailcount > 0&&bombacount+bombafailcount==bombagoal) || (bombbfailcount > 0&&bombbcount+bombbfailcount==bombbgoal))
            {
                FailCanvas.SetActive(true);
            }
            */
        }

        public void bombAdefusedsuccess() //increase the number of defusing BombA successfully
        {
            bombacount++;
        }
        public void bombBdefusedsuccess()//increase the number of defusing BombB successfully
        {
            bombbcount++;
        }
        public void bombAdefusedfail()//increase the number of defusing BombA unsuccessfully
        {
            bombafailcount++;
        }
        public void bombBdefusedfail()//increase the number of defusing BombB unsuccessfully
        {
            bombbfailcount++;
        }
        public void resetcount()//reset paramaters
        {
            bombacount = 0;
            bombbcount = 0;
            bombafailcount = 0;
            bombbfailcount = 0;
        }
        public void setbombA(int number)//setting bombagoal 
        {
            bombagoal = number;
            Debug.Log(bombagoal);
        }
        public void setbombB(int number)//setting bombbgoal
        {
            bombbgoal = number;
        }

        public int bombacountGetter()//return the bombacount
        {
            return bombacount;
        }

        public int bombbcountGetter()//return the bombbcount
        {
            return bombbcount;
        }
        public int bombafailcountGetter()//return the bombafailcount
        {
            return bombafailcount;
        }
        public int bombbfailcountGetter()//return the bombbfailcount
        {
            return bombbfailcount;
        }
        public int bombagoalGetter()//return the bombagoal
        {
            return bombagoal;
        }

        public int bombbgoalGetter()//return the bombbgoal
        {
            return bombbgoal;
        }

        void transmitdata()//transfer data for second scene
        {
            PlayerPrefs.SetInt("bombacount", bombacount);
            PlayerPrefs.SetInt("bombbcount", bombbcount);
            PlayerPrefs.SetInt("bombafailcount", bombafailcount);
        }
    }
}