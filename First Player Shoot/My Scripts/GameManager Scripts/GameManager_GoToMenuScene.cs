﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 *implementing the function of controling the CanvasMenu 
 */
namespace DRDC1
{
    public class GameManager_GoToMenuScene : MonoBehaviour
    {
        private GameManager_Master gameManagerMaster;

        private void OnEnable()//Add GoToMenuScene to GoToMenuSceneEvent
        {
            SetInitialReferences();
            gameManagerMaster.GoToMenuSceneEvent += GoToMenuScene;
        }

        private void OnDisable()//Delete GoToMenuScene from GoToMenuSceneEvent
        {
            gameManagerMaster.GoToMenuSceneEvent -= GoToMenuScene;
        }

        void SetInitialReferences()//initialization
        {
            gameManagerMaster = GetComponent<GameManager_Master>();
        }

        void GoToMenuScene()//Opening the CanvasMenu
        {
            Application.LoadLevel(0);
        }
    }
}


