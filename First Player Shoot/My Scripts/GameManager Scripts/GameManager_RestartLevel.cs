﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
 * implementing the function of restarting
 */
namespace DRDC1
{
    public class GameManager_RestartLevel : MonoBehaviour
    {
        private GameManager_Master gameMasterManager;

        private void OnEnable()//Add RestartLevel to RestartLevelEvent
        {
            SetInitialReferences();
            gameMasterManager.RestartLevelEvent += RestartLevel;
        }

        private void OnDisable()//Delete RestartLevel from RestartLevelEvent
        {
            gameMasterManager.RestartLevelEvent -= RestartLevel;
        }

        private void SetInitialReferences()//Initialization
        {
            gameMasterManager = GetComponent<GameManager_Master>();
        }

        private void RestartLevel()//Restart function, all paramaters will be set to initialize state; the scene will be set to Game Scene
        {
            SceneManager.LoadScene(0);
        }
    }
}


