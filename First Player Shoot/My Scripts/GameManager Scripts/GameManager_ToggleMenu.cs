﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Controlling the landing page of every scene
 */
namespace DRDC1
{
    public class GameManager_ToggleMenu : MonoBehaviour
    {
        private GameManager_Master gameManagerMaster;
        public GameObject menu;
        // Use this for initialization
        void Start()
        {
            ToggleMenu();
        }

        // Update is called once per frame
        /*
        void Update()
        {
            CheckForMenuToggleRequest();
        }
        */
        private void OnEnable()//add ToggleMenu to GameOverEvent
        {
            SetInitialReferences();
            gameManagerMaster.GameOverEvent += ToggleMenu;
            //gameManagerMaster.MenuToggleEvent += ToggleMenu;
            //gameManagerMaster.GoToMenuSceneEvent += ToggleMenu;
        }

        private void OnDisable()//delete ToggleMenu from GameOverEvent
        {
            gameManagerMaster.GameOverEvent -= ToggleMenu;
            //gameManagerMaster.GoToMenuSceneEvent -= ToggleMenu;
        }

        void SetInitialReferences()//initialization
        {
            gameManagerMaster = GetComponent<GameManager_Master>();
        }

        void CheckForMenuToggleRequest()//don't need any more
        {
            if(Input.GetKeyUp(KeyCode.Escape)&&!gameManagerMaster.isGameOver&&!gameManagerMaster.isInventoryUIOn)
            {
                ToggleMenu();
                Debug.LogWarning("sdafasdf");
            }
        }

        public void ToggleMenu()//open or close the landing page
        {
            if(menu!=null)
            {
                //Debug.Log("callmenu");
                //Debug.Log(menu.active);
                menu.SetActive(!menu.activeSelf);
                gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
                gameManagerMaster.CallEventMenuToggle();
            }
            else
            {
                Debug.LogWarning("assign something");
            }
        }

        public void ToggleMenuEvent()//don't need any more
        {
            if (menu != null)
            {
                //Debug.Log("callmenu");
                //Debug.Log(menu.active);
                //menu.SetActive(!menu.activeSelf);
                //gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
                gameManagerMaster.CallEventMenuToggle();
            }
            else
            {
                Debug.LogWarning("assign something");
            }
        }
        public void ToggleStartMenu()//start page
        {
            if (menu != null)
            {
                menu.SetActive(true);
                gameManagerMaster.isMenuOn = true;
            }
            else
            {
                Debug.LogWarning("assign something");
            }
        }

        public void ToggleCloseMenu()//close page
        {
            if (menu != null&& !gameManagerMaster.isGameOver)
            {
                menu.SetActive(false);
                gameManagerMaster.isMenuOn = false;
            }
            else
            {
                Debug.LogWarning("assign something");
            }
        }


    }
}


