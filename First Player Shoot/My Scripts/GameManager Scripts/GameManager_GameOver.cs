﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * implementing the function of controling the canvas of GameOver 
 */
namespace DRDC1
{
    public class GameManager_GameOver : MonoBehaviour
    {
        private GameManager_Master gameManagerMaster;
        public GameObject panelGameOver;

        private void OnEnable()//Adding TurnOnGameOverPanel to the GameOverEvent
        {
            SetInitialReferences();
            gameManagerMaster.GameOverEvent += TurnOnGameOverPanel;
        }

        private void OnDisable()//Deleting TurnOnGameOverPanel from the GameOverEvent
        {
            gameManagerMaster.GameOverEvent -= TurnOnGameOverPanel;
        }

        private void SetInitialReferences()//Initialization
        {
            gameManagerMaster = GetComponent<GameManager_Master>();
        }

        private void TurnOnGameOverPanel()//Turning on GameOver Canvas
        {
            if(panelGameOver!=null)
            {
                Debug.Log("game over");
                gameManagerMaster.isGameOver = true;
                panelGameOver.SetActive(true);
            }
        }
    }
}


