﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Controlling the Setting Canvas
 */
namespace DRDC1{
    public class GameManager_ToggleSetting : MonoBehaviour
    {
        public bool hasInventory;
        public GameObject SettingUI;
        public string toggleInventoryButton;
        private GameManager_Master gameManagerMaster;
        // Use this for initialization
        void Start()
        {
            SetInitialReferences();
        }

        // Update is called once per frame
        void Update()
        {

        }
        void SetInitialReferences()//initialization
        {
            gameManagerMaster = GetComponent<GameManager_Master>();

            if (toggleInventoryButton == "")
            {
                Debug.LogWarning("no name");
                this.enabled = false;
            }
        }

        public void ToggleOpenSettingUI()//open the setting page
        {
            if (SettingUI != null&& !gameManagerMaster.isGameOver)
            {
                SettingUI.SetActive(true);
            }
        }

        public void ToggleCloseSettingUI()//close the setting page
        {
            if (SettingUI != null)
            {
                SettingUI.SetActive(false);
            }
        }
    }

}
