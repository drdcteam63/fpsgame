﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
/*
 * don't need any more
 */
namespace DRDC1
{
    public class GameManager_TogglePlayer : MonoBehaviour
    {
        public FirstPersonController playerController;
        private GameManager_Master gameMasterManager;

        private void OnEnable()
        {
            SetInitialReferences();
            gameMasterManager.MenuToggleEvent += TogglePlayerController;
            gameMasterManager.InventoryUIToggleEvent += TogglePlayerController;
        }

        private void OnDisable()
        {
            gameMasterManager.MenuToggleEvent -= TogglePlayerController;
            gameMasterManager.InventoryUIToggleEvent -= TogglePlayerController;
        }

        void SetInitialReferences()
        {
            gameMasterManager = GetComponent<GameManager_Master>();
        }

        void TogglePlayerController()
        {
            if(playerController!=null)
            {
                playerController.enabled = !playerController.enabled;
            }
        }
    }
}


