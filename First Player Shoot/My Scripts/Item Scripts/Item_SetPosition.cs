﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
set position for item when it is picked up
*/
namespace DRDC1{
public class Item_SetPosition : MonoBehaviour {
		private Item_Master itemMaster;
		public Vector3 itemLocalPosition;
//set position of item at the beginning of the game
		void Start(){
			SetPositionOnPlayer ();
		}
//set up
		void OnEnable(){
			SetInitialReferences ();
			SetPositionOnPlayer ();
			itemMaster.EventObjectPickup += SetPositionOnPlayer;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectPickup -= SetPositionOnPlayer;
		}
//initialization
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		}
//set up position of item on player
		void SetPositionOnPlayer(){
			if (transform.root.CompareTag ("Player")) {
				transform.localPosition = itemLocalPosition;
			}
		}

}
}
