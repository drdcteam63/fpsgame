﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement animation of item pick up
*/
namespace DRDC1{
public class Item_Animator : MonoBehaviour {
		private Item_Master itemMaster;
		public Animator myAnimator;
//set up
		void OnEnable(){
			SetInitialReferences ();
			itemMaster.EventObjectThrow += DisableMyAnimator;
			itemMaster.EventObjectPickup += EnableMyAnimator;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectThrow -= DisableMyAnimator;
			itemMaster.EventObjectPickup -= EnableMyAnimator;
}
//initialization
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		}
//enable animation for item pick up
		void EnableMyAnimator(){
			if (myAnimator != null) {
				myAnimator.enabled = true;
			}
		}
//disable animation for item pick up
		void DisableMyAnimator(){
			if (myAnimator != null) {
				myAnimator.enabled = false;
			}
		}
	}
}
