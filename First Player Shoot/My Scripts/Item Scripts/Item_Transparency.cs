﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
set up material for item including primary material and transpanent material
*/
namespace DRDC1{
public class Item_Transparency : MonoBehaviour {
		private Item_Master itemMaster;
		public Material transparentMat;
		private Material primaryMat;
//initialization and get starting material/transparency-100%
		void Start()
		{
			SetInitialReferences ();
			CaptureStartingMaterial ();
		}

//set up
		void OnEnable()
		{
			SetInitialReferences ();
			itemMaster.EventObjectPickup += SetToTransparentMaterial;
			itemMaster.EventObjectThrow += SetToPrimaryMaterial;
		}
//clean up
		void OnDisable()
		{
			itemMaster.EventObjectPickup -= SetToTransparentMaterial;
			itemMaster.EventObjectThrow -= SetToPrimaryMaterial;
		}


//initializaiton
		void SetInitialReferences()
		{
			itemMaster = GetComponent<Item_Master> ();
		}
//get item material
		void CaptureStartingMaterial()
		{
			primaryMat = GetComponent<Renderer> ().material;
		}
//set material to primary material
		void SetToPrimaryMaterial()
		{
			GetComponent<Renderer> ().material = primaryMat;
		}
//set material to transparent material
		void SetToTransparentMaterial()
		{
			GetComponent<Renderer> ().material = transparentMat;
		}

}
}
