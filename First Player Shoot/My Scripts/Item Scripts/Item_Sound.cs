﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
set up sound when the item is threw up
*/
namespace DRDC1{
public class Item_Sound : MonoBehaviour {
		private Item_Master itemMaster;
		public float defaultVolume;
		public AudioClip throwSound;
//set up
		void OnEnable(){
			SetInitialReferences ();
			itemMaster.EventObjectThrow += PlayThrowSound;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectThrow -= PlayThrowSound;
		}
///initializaiton
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		}
//play throwing sound
		void PlayThrowSound(){
			if (throwSound != null) {
				AudioSource.PlayClipAtPoint (throwSound, transform.position, defaultVolume);
			}
		}

}
}
