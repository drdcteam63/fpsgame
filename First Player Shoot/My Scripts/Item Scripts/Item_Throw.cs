﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
implement item throw action
*/
namespace DRDC1{
public class Item_Throw : MonoBehaviour {
		private Item_Master itemMaster;
		private Transform myTransform;
		private Rigidbody myRigidbody;
		private Vector3 throwDirection;

		public bool canBeThrown;
		public string throwButtonName;
		public float throwForce;
        public static ThrowControl throwbutton;
//initializaiton at the beginning of the game
        void Start(){
			SetInitialReferences ();
		}
//update() is called  every frame, check if the throw button is pressed down
		void Update(){
            //Debug.Log(throwbutton);
            CheckForThrowInput ();
		}
//initializaiton
		void SetInitialReferences(){
            itemMaster = GetComponent<Item_Master> ();
			myTransform = transform;
			myRigidbody = GetComponent<Rigidbody> ();
		}
//if the throw button is pressed down, call throw fucntion
		void CheckForThrowInput(){
            if (throwButtonName != null) {
                //Debug.Log(ThrowControl.isthrowbutton);
                if (ThrowControl.isthrowbutton&& Time.timeScale>0&&canBeThrown&&
					myTransform.root.CompareTag(GameManager_References._playerTag))
				{
					CarryOutThrowActions();
				}
				}
		}
//throw the item
		void CarryOutThrowActions(){
			throwDirection = myTransform.parent.forward;
			myTransform.parent = null;
			itemMaster.CallEventObjectThrow ();
			HurlItem ();

		}
//throw the item
		void HurlItem(){
			myRigidbody.AddForce (throwDirection * throwForce, ForceMode.Impulse);

		}
}
}
