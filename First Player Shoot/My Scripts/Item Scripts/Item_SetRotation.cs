﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
set rotation of item
*/
namespace DRDC1{
public class Item_SetRotation : MonoBehaviour {
		private Item_Master itemMaster;
		public Vector3 itemLocalRotation;
//set up and call initialization function
		void OnEnable()
		{
			SetInitialReferences ();
			itemMaster.EventObjectPickup+= SetRotationOnPlayer;
		}
//clean up
		void OnDisable()
		{
			itemMaster.EventObjectPickup -= SetRotationOnPlayer;
		}
//call initialization function at the beginning out of concurrency problem
		void Start()
		{
			SetInitialReferences ();
		}
//initializaiton
		void SetInitialReferences()
		{
			itemMaster = GetComponent<Item_Master> ();
		}
//set rotation of item
		void SetRotationOnPlayer()
		{
			if (transform.root.CompareTag (GameManager_References._playerTag)) {
				transform.localEulerAngles = itemLocalRotation;
			}
		}
			

}
}
