﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
toggle item's collider depenging on its status
*/
namespace DRDC1{
public class Item_Colliders : MonoBehaviour {
		private Item_Master itemMaster;
		public Collider[] colliders;
		public PhysicMaterial myPhysicMaterial;
//check if the item is in inventory at the beginning of game, if yes, disable item's collider
		void Start()
		{
			CheckIfStartsInInventory ();
		}
//set up
		void OnEnable(){
			SetInitialReferences ();
			CheckIfStartsInInventory ();
			itemMaster.EventObjectThrow += EnableColliders;
			itemMaster.EventObjectPickup += DisableColliders;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectThrow -= EnableColliders;
			itemMaster.EventObjectPickup -= DisableColliders;
		}
//initializaiton
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		}
//check if the item is in inventory at the beginning of game, if yes, disable item's collider
		void CheckIfStartsInInventory(){
			if (transform.root.CompareTag ("Player")) {
				DisableColliders ();
			}
		}
			
//enable item's collider
		void EnableColliders(){
			if (colliders.Length > 0) {
				foreach(Collider col in colliders){
					col.enabled = true;
					if (myPhysicMaterial != null) {
						col.material = myPhysicMaterial;
					}
				}
			}
		}
//diable item's collider
		void DisableColliders(){
			if (colliders.Length > 0) {
				foreach(Collider col in colliders){
					col.enabled = false;
			
				}
			}
		}
}
}