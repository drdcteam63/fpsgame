﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
set up item layer
*/
namespace DRDC1{
public class Item_SetLayer : MonoBehaviour {
		private Item_Master itemMaster;
		public string itemThrowLayer;
		public string itemPickupLayer;
//set layer for item at the beginning 
		void Start()
		{
			SetLayerOnEnable ();
		}
//set up
		void OnEnable(){
			SetInitialReferences ();
			SetLayerOnEnable ();
			itemMaster.EventObjectPickup += SetItemToPickupLayer;
			itemMaster.EventObjectThrow += SetItemToThrowLayer;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectPickup -= SetItemToPickupLayer;
			itemMaster.EventObjectThrow -= SetItemToThrowLayer;
		}
//initialization
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();

		}
//set correct layer for each item 
		void SetLayerOnEnable(){
			if (itemPickupLayer == "") {
				itemPickupLayer="Weapon";
			}

			if (itemThrowLayer == "") {
				itemPickupLayer="Item";
			}

			if (transform.root.CompareTag ("Player")) {
				SetItemToPickupLayer ();
			} else {
				SetItemToThrowLayer ();
			}
		}
//set the layer when the item is threw up
		void SetItemToThrowLayer(){
			SetLayer (transform, itemThrowLayer);
		}
//set the layer for item when the item is picked up
		void SetItemToPickupLayer(){
			SetLayer (transform, itemPickupLayer);
		}

//set layer function
		void SetLayer(Transform tForm,string itemLayerName){
			tForm.gameObject.layer = LayerMask.NameToLayer (itemLayerName);
			foreach (Transform child in tForm) {
				SetLayer (child, itemLayerName);
			}
		}
}
}
