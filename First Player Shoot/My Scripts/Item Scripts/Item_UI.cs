﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
set up item UI
*/
namespace DRDC1{
public class Item_UI : MonoBehaviour {
		private Item_Master itemMaster;
		public GameObject myUI;

//set up
		void OnEnable(){
			SetInitialReferences ();
			itemMaster.EventObjectPickup += EnableMyUI;
			itemMaster.EventObjectThrow += DisableMyUI;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectPickup -= EnableMyUI;
			itemMaster.EventObjectThrow -= DisableMyUI;
		}
//initialization
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		}
//enable item UI
		void EnableMyUI(){
			if(myUI !=null){
				myUI.SetActive (true);
			}
		}
//diable item UI
		void DisableMyUI(){
			if(myUI !=null){
				myUI.SetActive (false);
			}

	}
	}
}
