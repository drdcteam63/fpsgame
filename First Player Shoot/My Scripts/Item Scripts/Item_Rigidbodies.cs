﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
set rigidbody for items
*/
namespace DRDC1{
public class Item_Rigidbodies : MonoBehaviour {
		private Item_Master itemMaster;
		public Rigidbody[] rigidBodies;
		//at the beginning, check if the item is in inverntory, if yes, set isKinematic to true
		void Start()
		{
			CheckIfStartsInInventory ();
		}
//set up
		void OnEnable(){
			SetInitialReferences ();
			CheckIfStartsInInventory ();
			itemMaster.EventObjectThrow += SetIsKinematicToFalse;
			itemMaster.EventObjectPickup += SetIsKinematicToTrue;
		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectThrow -= SetIsKinematicToFalse;
			itemMaster.EventObjectPickup -= SetIsKinematicToTrue;
		
		}
//initialization
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		}
//at the beginning, check if the item is in inverntory, if yes, set isKinematic to true
		void CheckIfStartsInInventory(){
			if (transform.root.CompareTag ("Player")) {
				SetIsKinematicToTrue ();
			}
		}
//set is kinematic to true
		void SetIsKinematicToTrue(){
			if (rigidBodies.Length > 0) {
				foreach (Rigidbody rBody in rigidBodies) {
					rBody.isKinematic = true;
				}
			}
		}
//set kinematic to false
		void SetIsKinematicToFalse(){
			if (rigidBodies.Length > 0) {
				foreach (Rigidbody rBody in rigidBodies) {
					rBody.isKinematic = false;
				}
			}
		}
}
}
