﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
implement item drop related effect
*/
namespace DRDC1{
public class Item_Drop : MonoBehaviour {
		private Item_Master itemMaster;
		public string dropButtionName;
		private Transform myTransform;
//initialization
		void SetInitialReferences()
		{
			itemMaster = GetComponent<Item_Master> ();
			myTransform = transform;
		}
//check if the drop button is pressed down, but in our current game version, we don't include this functionality
		void CheckForDropInput()
		{
			if (Input.GetButtonDown (dropButtionName) && Time.timeScale > 0 &&
			   myTransform.root.CompareTag (GameManager_References._playerTag)) {
				myTransform.parent = null;
				itemMaster.CallEventObjectThrow ();
			}
		}
//initialization function will be called at the beginning to solver concurrency problem
		void Start()
		{
			SetInitialReferences ();
		}
//update() is called every frame to check if the drop item button is pressed or not
		void Update()
		{
			CheckForDropInput ();
		}

}
}
