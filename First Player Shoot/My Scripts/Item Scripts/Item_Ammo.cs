﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement item ammo which can be picked up for related gun
*/
namespace DRDC1{
public class Item_Ammo : MonoBehaviour {
		private Item_Master itemMaster;
		private GameObject playerGo;
		public string ammoName;
		public int quantity;//item ammo amount 
		public bool isTriggerPickup;//if the ammo can be picked up by touching it;on-yes
//set up
		void OnEnable(){
			SetInitialReferences ();
			itemMaster.EventObjectPickup += TakeAmmo;

		}
//clean up
		void OnDisable(){
			itemMaster.EventObjectPickup -= TakeAmmo;
		}
//if the isTriggerPickup is toggled on, the player can pick up the ammo by toucking it
		void OnTriggerEnter(Collider other){
			if (other.CompareTag (GameManager_References._playerTag) && isTriggerPickup) {
				TakeAmmo ();
			}
		}
//initializaiton
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
			playerGo = GameManager_References._player;

			if (isTriggerPickup) {
				if (GetComponent<Collider> () != null) {
					GetComponent<Collider> ().isTrigger = true;
				}

				if (GetComponent<Rigidbody> () != null) {
					GetComponent<Rigidbody> ().isKinematic = true;
				}
			}
		}
//take ammo can call rammo pick up event to update related stuff
		void TakeAmmo(){
            SetInitialReferences();
            playerGo.GetComponent<Player_Master> ().CallEventPickedUpAmmo (ammoName, quantity);
			Destroy (gameObject);
			
			

}
}
}
