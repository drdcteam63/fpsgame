﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement item pickup functionality
*/
namespace DRDC1{
public class Item_Pickup : MonoBehaviour {
		private Item_Master itemMaster;
		private Transform myTransform;

//initialization at the beginning
		void Awake(){
			SetInitialReferences ();
		}
//set up
		void OnEnable(){
			itemMaster.EventPickupAction += CarryOutPickupActions;
		}
//clean up
		void OnDisable(){
			itemMaster.EventPickupAction -= CarryOutPickupActions;
		}
//initialization
		void SetInitialReferences(){
			itemMaster = GetComponent<Item_Master> ();
		
		}
//call pick up event and preset for picking up items
		void CarryOutPickupActions(Transform tParent){
			transform.SetParent (tParent);
			itemMaster.CallEventObjectPickup ();
			transform.gameObject.SetActive (false);
		
		}
}
}
