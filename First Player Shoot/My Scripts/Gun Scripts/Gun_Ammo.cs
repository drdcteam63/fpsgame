﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*   
implement gun ammo change     
*/
namespace DRDC1{
public class Gun_Ammo : MonoBehaviour {
		private Player_Master playerMaster;
		private Gun_Master gunMaster;
		private Player_AmmoBox ammoBox;
		private Animator myAnimator;

		public int clipSize;
		public int currentAmmo;
		public string ammoName;
		public float reloadTime;

//initiation
		void OnEnable()
		{
			SetInitialReferences ();
			StartingSanityCheck ();
			CheckAmmoStatus ();
			gunMaster.EventPlayerInput += DeductAmmo;
			gunMaster.EventPlayerInput += CheckAmmoStatus;
			gunMaster.EventRequestReload += TryToReload;
			gunMaster.EventGunNotUsable += TryToReload;
			gunMaster.EventRequestGunReset += ResetGunReloading;

			if (playerMaster != null) {
				playerMaster.EventAmmoChanged += UIAmmoUpdateRequest;
			}
			if (ammoBox != null) {
				StartCoroutine (UpdateAmmoUIWhenEnabling ());
			}
		}
//clean up 
		void OnDisable()
		{
			gunMaster.EventPlayerInput -= DeductAmmo;
			gunMaster.EventPlayerInput -= CheckAmmoStatus;
			gunMaster.EventRequestReload -= TryToReload;
			gunMaster.EventGunNotUsable -= TryToReload;
			gunMaster.EventRequestGunReset -= ResetGunReloading;

			if (playerMaster != null) {
				playerMaster.EventAmmoChanged -= UIAmmoUpdateRequest;
			}
		}

		void Start(){
			SetInitialReferences ();
			StartCoroutine (UpdateAmmoUIWhenEnabling ());
			if (playerMaster != null) {
				playerMaster.EventAmmoChanged += UIAmmoUpdateRequest;
			}
		}

		void SetInitialReferences()
		{
			gunMaster=GetComponent<Gun_Master> ();
			if (GetComponent<Animator> () != null) {
				myAnimator = GetComponent<Animator> ();
			}

			if (GameManager_References._player != null) {
			
				playerMaster = GameManager_References._player.GetComponent<Player_Master> ();
				ammoBox = GameManager_References._player.GetComponent<Player_AmmoBox> ();

				}
		}
//deduct ammo by one and request ammo UI change
		void DeductAmmo()
		{
			currentAmmo--;
			UIAmmoUpdateRequest ();
		}
//reload the gun
		void TryToReload()
		{
			for(int i=0;i<ammoBox.typesOfAmmunition.Count;i++){
				if (ammoBox.typesOfAmmunition [i].ammoName == ammoName) {
					if (ammoBox.typesOfAmmunition [i].ammoCurrentCarried > 0 &&
					   currentAmmo != clipSize &&
					   !gunMaster.isReloading) {
						gunMaster.isReloading = true;
						gunMaster.isGunLoaded = false;
						if (myAnimator != null) {
							myAnimator.SetTrigger ("Reload");
						} else {
							StartCoroutine (ReloadWithoutAnimation ());
						}
					}
					break;
				}
			}

		}
//check current ammo amount
		void CheckAmmoStatus()
			{
			if (currentAmmo <= 0) {
				currentAmmo = 0;
				gunMaster.isGunLoaded = false;
			}	
			else if(currentAmmo>0){
				gunMaster.isGunLoaded = true;
			}
			}
//check current ammo amount and limit it to clip size
		void StartingSanityCheck()
		{
			if (currentAmmo > clipSize) {
				currentAmmo = clipSize;
			}
		}
//request ammo UI change
		void UIAmmoUpdateRequest()
		{
			for (int i = 0; i < ammoBox.typesOfAmmunition.Count; i++) {
				if (ammoBox.typesOfAmmunition [i].ammoName == ammoName) {
					gunMaster.CallEventAmmoChanged (currentAmmo, ammoBox.typesOfAmmunition [i].ammoCurrentCarried);
					break;
				}
			}
		}
//reset reloading status for gun
		void ResetGunReloading()
		{
			gunMaster.isReloading = false;
			CheckAmmoStatus ();
			UIAmmoUpdateRequest ();
		}
//update ammo amount for current and carried when reloading finished
		public void OnReloadComplete()
		{
			//Attempt to add ammo to current
			for (int i = 0;i < ammoBox.typesOfAmmunition.Count;i++) {
				if (ammoBox.typesOfAmmunition [i].ammoName == ammoName) {
					int ammoTopUp = clipSize - currentAmmo;
					if (ammoBox.typesOfAmmunition [i].ammoCurrentCarried >= ammoTopUp) {
						currentAmmo += ammoTopUp;
						ammoBox.typesOfAmmunition [i].ammoCurrentCarried -= ammoTopUp;
					} else if (ammoBox.typesOfAmmunition [i].ammoCurrentCarried < ammoTopUp &&
					        ammoBox.typesOfAmmunition [i].ammoCurrentCarried != 0) {
						currentAmmo += ammoBox.typesOfAmmunition [i].ammoCurrentCarried;
						ammoBox.typesOfAmmunition [i].ammoCurrentCarried = 0;
					}
					break;

				}
			}
		ResetGunReloading();
		}

//wait for reload animation finished	
		IEnumerator ReloadWithoutAnimation()
		{
			yield return new WaitForSeconds (reloadTime);
			OnReloadComplete ();
		}
//wait sometime and then update ammo UI
		IEnumerator UpdateAmmoUIWhenEnabling()
		{
			yield return new WaitForSeconds (0.05f);
			UIAmmoUpdateRequest ();
		}
}
}
