﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implment gun damage effect when other stuff are hit
*/
namespace DRDC1{
public class Gun_ApplyDamage : MonoBehaviour {
		private Gun_Master gunMaster;
		public int damage=10;//damage value for one shot on stuff
//set up when the script is enabled
		void OnEnable(){
			SetInitialReference ();
			gunMaster.EventShotEnemy += ApplyDamage;
            gunMaster.EventShotDefault += ApplyDamage;
		}
//clean up when disabled
		void OnDisable(){
			gunMaster.EventShotEnemy -= ApplyDamage;
            gunMaster.EventShotDefault -= ApplyDamage;
        }
//initiation
		void SetInitialReference(){
			gunMaster = GetComponent<Gun_Master> ();
		}
//apply damage on stuff which gets shot
	void ApplyDamage(RaycastHit hitPosition,Transform hitTransform){
            //Debug.Log("ApplyDamage");
            //Debug.Log("hitTans:"+hitTransform);
            hitTransform.SendMessage("ProcessDamage", damage, SendMessageOptions.DontRequireReceiver);
            hitTransform.SendMessage("CallEventPlayerHealthDeduction", damage, SendMessageOptions.DontRequireReceiver);
            hitTransform.root.SendMessage("SetMyAttacter", transform.root, SendMessageOptions.DontRequireReceiver);
        }


    }
}

