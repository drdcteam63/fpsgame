﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
implement ammo UI change 
*/
namespace DRDC1
{
    public class Gun_AmmoUI : MonoBehaviour
    {
        public InputField currentAmmoField;
        public InputField carreiedAmmoField;
        private Gun_Master gunMaster;
//initiate when the script is enabled
        void OnEnable()
        {
            SetInitialReferences();
            gunMaster.EventAmmoChanged += UpdateAmmoUI;
        }
//clean up when the scipt is disabled
        void OnDisable()
        {
            gunMaster.EventAmmoChanged -= UpdateAmmoUI;
        }
//initiation
        void SetInitialReferences()
        {
            gunMaster = GetComponent<Gun_Master>();
        }
//update ammo UI[current ammo amount&carried ammo amount]
        void UpdateAmmoUI(int currentAmmo, int carriedAmmo)
        {
            if (currentAmmoField != null)
            {
                currentAmmoField.text = currentAmmo.ToString();
            }
            if (carreiedAmmoField != null)
            {
                carreiedAmmoField.text = carriedAmmo.ToString();
            }

        }

    }
}

