﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
implement gun muzzle flash effect for gun when gun fires
*/
namespace DRDC1{
public class Gun_MuzzleFlash : MonoBehaviour {
		public ParticleSystem muzzleFlash;
		private Gun_Master gunMaster;
//set up
		void OnEnable(){
			SetInitialReferences ();
			gunMaster.EventPlayerInput += PlayMuzzleFlash;
            gunMaster.EventNpcInput += PlayMuzzleFlashForNpc;
        }
//clean up
		void OnDisable(){

			gunMaster.EventPlayerInput -= PlayMuzzleFlash;
            gunMaster.EventNpcInput -= PlayMuzzleFlashForNpc;
        }
        //initialization
		void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master> ();
		}
//play muzzle flash when gun fires for player
		void PlayMuzzleFlash(){
			if (muzzleFlash != null) {
				muzzleFlash.Play ();
			}
		}
//play muzzle flash when gun fires for enemies
        void PlayMuzzleFlashForNpc(float dummy)
        {
            if (muzzleFlash != null)
            {
                muzzleFlash.Play();
            }
        }
    }
}
