﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
implement gun shooting when shoot button is pressed down
*/
namespace DRDC1{
public class Gun_StandardInput : MonoBehaviour {

		private Gun_Master gunMaster;
	private float nextAttack;
	public float attackRate=0.5f;
	private Transform myTransform;
	public bool isAutomatic;
	public bool hasBurstFire;
	private bool isBurstFireActive;
	public string attackButtonName;
	public string reloadButtonName;
	public string burstFireButtonName;
        public ShootControl shootbutton;
        public bool isshootbutton=true;
//call initialization function at the beginning to solve concurrency problem
        void Start(){
		SetInitialReferences ();
            
	}
//update() is called every frame
	public void Update(){
        CheckIfWeaponShouldAttack ();
		CheckForBurstFireToggle ();
		CheckForReloadRequest ();
            //Debug.Log("Gunaksdjlaiwjldaksnsfhaslnflhwoeih");
        }
//initialization
	void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master>();
		myTransform = transform;
		gunMaster.isGunLoaded = true;//so the player can attempt shooting right away
	}
//check if player wants to shoot again
	void AttemptAttack(){
		nextAttack = Time.time + attackRate;
        isshootbutton = false;
        if (gunMaster.isGunLoaded) {
			//Debug.Log ("shooting");
			gunMaster.CallEventPlayerInput ();
		}
			else{
				gunMaster.CallEventGunNotUsable();
			}
	}
//check if there is reload request from player
	void CheckForReloadRequest(){
			if(Input.GetButtonDown(reloadButtonName)&& Time.timeScale > 0 &&
				myTransform.root.CompareTag (GameManager_References._playerTag))
				{
			gunMaster.CallEventRequestReload();
		}

	}
//check if burst fire effect is on or not
	void CheckForBurstFireToggle(){
		if (Input.GetButtonDown (burstFireButtonName) && Time.timeScale > 0 &&
		   myTransform.root.CompareTag (GameManager_References._playerTag)) {
			//Debug.Log ("Burst Fire Toggled");
			isBurstFireActive = !isBurstFireActive;
			gunMaster.CallEventToggleBurstFire ();
		}
	}
//check if weapon could attack again
	void CheckIfWeaponShouldAttack(){
		if (Time.time > nextAttack && Time.timeScale > 0 &&
		    myTransform.root.CompareTag (GameManager_References._playerTag)) {
			if (isAutomatic && !isBurstFireActive) {
                    if (shootbutton.isshootbutton) {
                        //Debug.Log ("Gull Auto");
                        AttemptAttack ();
                    }
                }
                else if (isAutomatic && isBurstFireActive)
                {
                    if (shootbutton.isshootbutton)
                    {
                        //Debug.Log("Burst");
                        StartCoroutine(RunBurstFire());
                    }
                }
                else if (!isAutomatic)
                {
                    if (shootbutton.isshootbutton)
                    {
                        AttemptAttack();
                    }
                }
            } 
	}
//check if player shoot just now
    public void CheckInput()
        {
            isshootbutton = true;
        }
//burst fire
	IEnumerator RunBurstFire(){
		AttemptAttack ();
		yield return new WaitForSeconds (attackRate);
		AttemptAttack ();
		yield return new WaitForSeconds (attackRate);
		AttemptAttack ();
	}
}
				}
