﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement gun sound effect of shooting
*/
namespace DRDC1{
public class Gun_Sounds : MonoBehaviour {
		private Gun_Master gunMaster;
		private Transform myTransform;
		public float shootVolume = 0.4f;
		public float reloadVolume = 0.5f;
		public AudioClip[] shootSound;
		public AudioClip reloadSound;
//set up
		void OnEnable()
		{
			SetInitialReferences ();
			gunMaster.EventPlayerInput += PlayShootSound;
            gunMaster.EventNpcInput += NpcPlayShootSound;
		}
//clean up
		void OnDisable(){
			gunMaster.EventPlayerInput -= PlayShootSound;
            gunMaster.EventNpcInput -= NpcPlayShootSound;
        }
//initialization
		void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master> ();
			myTransform = transform;
		}
//play shoot sound
		void PlayShootSound(){
			if (shootSound.Length > 0) {
				int index = Random.Range (0, shootSound.Length);
				AudioSource.PlayClipAtPoint (shootSound [index], myTransform.position, shootVolume);
			}
		}
//play reload sound
		public void PlayReloadSound()//Called by the reload animation event
		{
			if (reloadSound != null) {
				AudioSource.PlayClipAtPoint (reloadSound, myTransform.position, reloadVolume);
			
			}
		}
//play enemies shoot sound
        void NpcPlayShootSound(float dummy)
        {
            PlayShootSound();
        }

}
}