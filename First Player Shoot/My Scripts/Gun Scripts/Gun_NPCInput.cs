﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 enemies fire the gun when they find the palyer
*/
namespace DRDC1{
public class Gun_NPCInput : MonoBehaviour {

	private Gun_Master gunMaster;
	private Transform myTranfrom;
	private RaycastHit hit;
	public LayerMask layersToDamage;
//set up
		void OnEnable(){
			SetInitialReferences ();
			gunMaster.EventNpcInput += NpcFireGun;
		}
//clean up
		void OnDisable(){
			gunMaster.EventNpcInput -= NpcFireGun;
		}

//initialization
		void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master> ();
			myTranfrom = transform;
		}
//npc fires the gun when they find the player
		public void NpcFireGun(float randomness){
			Vector3 startPosition = new Vector3 (Random.Range (-randomness, randomness), Random.Range (-randomness, randomness), 0.5f);

			if (Physics.Raycast (myTranfrom.TransformPoint (startPosition), myTranfrom.forward, out hit, GetComponent<Gun_Shoot> ().range,layersToDamage)) {
				if (hit.transform.GetComponent<NPC_TakeDamage> () != null || hit.transform == GameManager_References._player.transform) {
					gunMaster. CallEventShotEnemy (hit, hit.transform);
				} else {
					gunMaster.CallEventShotDefault (hit, hit.transform); 
				}
			}
		}
}
}
