﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement gun force effect when something gets hit by bullet
*/
namespace DRDC1{
public class Gun_ApplyForce : MonoBehaviour {
		private Gun_Master gunMaster;
		private Transform myTranform;
		public float forceToApply=300;
//set up
		void OnEnable()
		{
			SetInitialReferences ();
			gunMaster.EventShotDefault += ApplyForce;
		}
//clean up
		void OnDisable()
		{
			gunMaster.EventShotDefault -= ApplyForce;
		}
//initiation
		void SetInitialReferences()
		{
			gunMaster = GetComponent<Gun_Master> ();
			myTranform = transform;
		}
//apply gun shooting force/bullet force on stuff which gets shot
		void ApplyForce(RaycastHit hitPosition,Transform hitTransform)
		{
			if (hitTransform.GetComponent<Rigidbody> () != null) {
				hitTransform.GetComponent<Rigidbody> ().AddForce (myTranform.forward * forceToApply, ForceMode.Impulse);

			}
		}

}
}
