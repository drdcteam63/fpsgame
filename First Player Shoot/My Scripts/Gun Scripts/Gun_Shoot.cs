﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*

implement real gun shooting, shooting at what is detected by raycast
*/
namespace DRDC1{
public class Gun_Shoot : MonoBehaviour {

	private Gun_Master gunMaster;
	private Transform myTransform;
	private Transform camTransform;
	private RaycastHit hit;
	public float range=400;//shoot range
	private float offsetFactor=7;
	private Vector3 startPosition;
//set up
	void OnEnable(){
		SetInitialReferences ();
		gunMaster.EventPlayerInput += OpenFire;
		gunMaster.EventSpeedCaptured += SetStartOfShootingPosition;
	}
//clean up
	void OnDisable(){
			gunMaster.EventPlayerInput -= OpenFire;
			gunMaster.EventSpeedCaptured -= SetStartOfShootingPosition;
	}
//initialization
	void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master> ();
			myTransform = transform;
			camTransform = myTransform.parent;
	}
//fire at what is detected within range
	void OpenFire(){
			//Debug.Log ("Open Fire called");
			if (Physics.Raycast (camTransform.TransformPoint (startPosition), camTransform.forward, out hit, range)) {
				if(hit.transform.GetComponent<NPC_TakeDamage>()!=null)
                {
                    gunMaster.CallEventShotEnemy(hit, hit.transform);
                }
                else
                {
                    gunMaster.CallEventShotDefault(hit, hit.transform);
                }
			}

	}
//the shooting accuracy will decrease with palyer's speed
	void SetStartOfShootingPosition(float playerSpeed){
			float offset=playerSpeed/offsetFactor;
			startPosition=new Vector3(Random.Range(-offset,offset),Random.Range(-offset,offset),1);
	}
		
	}
}

