﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement gun hit effects
*/
namespace DRDC1{
public class Gun_HitEffects : MonoBehaviour {

		private Gun_Master gunMaster;
		public GameObject defaultHitEffect;
		public GameObject enemyHitEfffect;
        static Object[] blood;
//set up
		void OnEnable(){
			SetInitialReferences ();
			gunMaster.EventShotDefault += SpawnDefaultHitEffect;
			gunMaster.EventShotEnemy += SpawnEnemyHitEfeect;
		}
//clean up
		void OnDisable(){
			gunMaster.EventShotDefault -= SpawnDefaultHitEffect;
			gunMaster.EventShotEnemy -= SpawnEnemyHitEfeect;
		}
//initiation
		void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master> ();
            blood = new Object[3];
		}
//produce hit effect when the bullet hits somewhere
		void SpawnDefaultHitEffect(RaycastHit hitPosition,Transform hitTransform){
			if (defaultHitEffect != null) {
                Quaternion quatAngle = Quaternion.LookRotation(hitPosition.normal);
                Instantiate(defaultHitEffect, hitPosition.point, quatAngle);
                
			}
		}
//produce hit effect when the bullet hits the enemy
		void SpawnEnemyHitEfeect(RaycastHit hitPosition,Transform hitTransform){
			if (enemyHitEfffect != null) {
                Quaternion quatAngle = Quaternion.LookRotation(hitPosition.normal);
                Object temp =Instantiate(enemyHitEfffect, hitPosition.point, quatAngle);
                if (blood[0] != null)
                {
                    Destroy(blood[0]);
                    Debug.Log("delete");
                }
                for (int i = 0; i < 2; i++)
                {
                    blood[i] = blood[i + 1];
                }
                blood[2] = temp;
            }
		}
}

}
