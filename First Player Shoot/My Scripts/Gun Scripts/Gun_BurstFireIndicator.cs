﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
implement burst fire effect when you toggle the burst fire button on
*/
namespace DRDC1{
public class Gun_BurstFireIndicator : MonoBehaviour {
		private Gun_Master gunMaster;
		public GameObject burstFireIndicator;//to set if the burst fire effect is on
		//set up
		void OnEnable()
		{
			SetInitialReferences ();
			gunMaster.EventToggleBurstFire += ToggleIndicator;
		}
//clean up
		void OnDisable()
		{
			gunMaster.EventToggleBurstFire -= ToggleIndicator;
		}
//initiation
		void SetInitialReferences()
		{
			gunMaster = GetComponent<Gun_Master> ();
		}
//toggle the burst fire on and off
		void ToggleIndicator()
		{
			if (burstFireIndicator != null) {
				burstFireIndicator.SetActive (!burstFireIndicator.activeSelf);
			}
		}
	
}
}
