﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*

implement gun reset request
*/
namespace DRDC1{
public class Gun_Reset : MonoBehaviour {

		private Gun_Master gunMaster;
		private Item_Master itemMaster;
//set up
		void OnEnable()
		{
			SetInitialReferences ();
			if (itemMaster != null) {
				itemMaster.EventObjectThrow += ResetGun;
			}
		}
//clean up
		void OnDisable(){
			if (itemMaster != null) {
				itemMaster.EventObjectThrow -= ResetGun;
			}
		}
//initialization
		void SetInitialReferences()
		{
			gunMaster = GetComponent<Gun_Master> ();
			if (GetComponent<Item_Master> () != null) {
				itemMaster = GetComponent<Item_Master> ();
			}
		}
//call gun reset request event
		void ResetGun()
		{
			gunMaster.CallEventRequestGunReset ();
		}
}
}
