﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
implement gun dynamic crosshair effect for aiming
*/
namespace DRDC1{
public class Gun_DynamicCrosshair : MonoBehaviour {
		private Gun_Master gunMaster;
		public Transform canvasDynamicCrosshair;  //crosshair image
		private Transform playerTransform;
		private Transform weaponCamera;
		private float playerSpeed;
		private float nextCaptureTime;
		private float captureInterval = 0.5f;//capture interval for calculating player's speed
		private Vector3 lastPosition;
		public Animator crosshairAnimator;
		public string weaponCameraName;
// call initiation function at the beginning
		void Start(){
			SetInitialReferences ();
		}
//update() is called every frame
		void Update(){
			CapturePlayerSpeed ();
			ApplySpeedToAnimation ();
		}
//initiation
		void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master> ();
			playerTransform = GameManager_References._player.transform;
			FindWeaponCamera (playerTransform);
			SetCameraOnDynamicCrosshairCanvas ();
			SetPlaneDistanceOnDynamicCrosshairCanvas ();
		}
//get player's run/walk speed
		void CapturePlayerSpeed(){
			if (Time.time > nextCaptureTime) {
				nextCaptureTime = Time.time + captureInterval;
				playerSpeed = (playerTransform.position - lastPosition).magnitude / captureInterval;
				lastPosition = playerTransform.position;
				gunMaster.CallEventSpeedCaptured (playerSpeed);
			}
		}
//the animation of crosshair will be affected by player's speed, the quicker the player, the larger the crosshair
		void ApplySpeedToAnimation(){
			if (crosshairAnimator != null) {
				crosshairAnimator.SetFloat ("Speed", playerSpeed);
			}		
		}
//get the weapon camera
		void FindWeaponCamera(Transform transformToSearchThrough){
			if (transformToSearchThrough != null) {
				if (transformToSearchThrough.name == weaponCameraName) {
					weaponCamera = transformToSearchThrough;
					return;
				}
				foreach (Transform child in transformToSearchThrough) {
					FindWeaponCamera (child);
				}
			}
		}
//set the camera to capture the crosshair
		void SetCameraOnDynamicCrosshairCanvas(){
			if (canvasDynamicCrosshair != null && weaponCamera != null) {
				canvasDynamicCrosshair.GetComponent<Canvas> ().renderMode = RenderMode.ScreenSpaceCamera;
				canvasDynamicCrosshair.GetComponent<Canvas> ().worldCamera = weaponCamera.GetComponent<Camera> ();

			}
		}
//set the distance between camera and crosshair
		void SetPlaneDistanceOnDynamicCrosshairCanvas()
		{
			if (canvasDynamicCrosshair != null) {
				canvasDynamicCrosshair.GetComponent<Canvas> ().planeDistance = 1;
			}
		}

}
}
