﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * control the events of Player
 */
namespace DRDC1{
public class Player_Master : MonoBehaviour {
	public delegate void GeneralEventHandler();
	public event GeneralEventHandler EventInventoryChanged;
	public event GeneralEventHandler EventHandsEmpty;
	public event GeneralEventHandler EventAmmoChanged;

	public delegate void AmmoPickUpEventHandler(string ammoName, int quantity);
	public event AmmoPickUpEventHandler EventPickedUpAmmo;

	public delegate void PlayerHealthHandler(int healthChange );
	public event PlayerHealthHandler EventPlayerHealthDeduction;
	public event PlayerHealthHandler EventPlayerHealthIncrease;
	//public NPC_Spawner npcspawner;

		void Start(){
			SetInitialReferences ();
			//npcspawner.SpawnEnemy ();
			//Debug.Log ("Initialize Succesfull");
		}


		void SetInitialReferences(){
			//npcspawner = GetComponent<NPC_Spawner> ();

		}

	public void CallEventInventoryChanged(){
		if (EventInventoryChanged != null) {
			EventInventoryChanged ();
		}
	}

	public void CallEventHandsEmpty(){
		if (EventHandsEmpty != null) {
			EventHandsEmpty ();
		}
	}

	public void CallEventAmmoChanged(){
		if (EventAmmoChanged != null) {
			EventAmmoChanged ();
		}
	}

	public void CallEventPickedUpAmmo(string ammoName, int quantity){
		if (EventPickedUpAmmo != null) {
			EventPickedUpAmmo (ammoName,quantity);
		}
	}

	public void CallEventPlayerHealthDeduction(int healthChange){
		if (EventPlayerHealthDeduction != null) {
			EventPlayerHealthDeduction(healthChange);
		}
	}

	public void CallEventPlayerHealthIncrease(int healthChange){
		if (EventPlayerHealthIncrease != null) {
			EventPlayerHealthIncrease(healthChange);
		}
	}
}
}
