﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * control the events of NPC
 */
public class NPC_Master : MonoBehaviour {
	public delegate void GeneralEventHandler();
	public event GeneralEventHandler EventNpcDie; 
	public event GeneralEventHandler EventNpcLowHealth; 
	public event GeneralEventHandler EventNpcHealthRecovered; 
	public event GeneralEventHandler EventNpcWalkAnim; 
	public event GeneralEventHandler EventNpcStruckAnim; 
	public event GeneralEventHandler EventNpcAttackAnim; 
	public event GeneralEventHandler EventNpcRecoveredAnim; 
	public event GeneralEventHandler EventNpcIdleAnim; 

	public delegate void HealthEventHandler(int health);
	public event HealthEventHandler EventNpcDeductHealth;
	public event HealthEventHandler EventNpcIncreaseHealth;

	//used for animation
	public string animationBoolPursuing="isPursuing";
	public string animationTriggerStruck="Struck";
	public string animationTriggerMelee="Attack";
	public string animationTriggerRecovered="Recovered";

	public void callEventNpcDie(){
		if(EventNpcDie!=null){
			EventNpcDie();
		}
	}

	public void callEventNpcLowHealth(){
		if(EventNpcLowHealth!=null){
			EventNpcLowHealth();
		}
	}

	public void callEventNpcHealthRecovered(){
		if(EventNpcHealthRecovered!=null){
			EventNpcHealthRecovered();
		}
	}

	public void callEventNpcWalkAnim(){
		if(EventNpcWalkAnim!=null){
            //Debug.Log("Event Waliking");
			EventNpcWalkAnim();
		}
	}

	public void callEventNpcStruckAnim(){
		if(EventNpcStruckAnim!=null){
			EventNpcStruckAnim();
		}
	}

	public void callEventNpcAttackAnim(){
		if(EventNpcAttackAnim!=null){
			EventNpcAttackAnim();
		}
	}

	public void callEventNpcRecoveredAnim(){
		if(EventNpcRecoveredAnim!=null){
			EventNpcRecoveredAnim();
		}
	}

	public void callEventNpcIdleAnim(){
		if(EventNpcIdleAnim!=null){
			EventNpcIdleAnim();
		}
	}

	public void callEventNpcDeductHealth(int health){
		if(EventNpcDeductHealth!=null){
			EventNpcDeductHealth(health);
		}
	}

	public void callEventNpcIncreaseHealth(int health){
		if(EventNpcIncreaseHealth!=null){
			EventNpcIncreaseHealth(health);
		}
	}
}
