//
//  NSDate+bridge.m
//  Unity-iPhone
//
//  Created by greay on 3/28/15.
//
//

#import "NSDate+bridge.h"

@implementation NSDate (conversion)

+ (NSDateFormatter *)bridgeFormatter
{
	static NSDateFormatter *format = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		format = [[NSDateFormatter alloc] init];
		// from C#:		2015-05-10T20:28:33+00:00
		// from objc:	2015-05-11T06:00:55-07:00
		format.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
	});
	return format;
}


+ (instancetype)dateFromBridgeString:(NSString *)stamp
{
	NSDate *date = [[self bridgeFormatter] dateFromString:stamp];
	return date;
}

- (NSString *)bridgeString
{
	NSString *string = [[NSDate bridgeFormatter] stringFromDate:self];
	return string;
}

@end
