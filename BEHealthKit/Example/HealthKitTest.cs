using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using BeliefEngine.HealthKit;
using System.Text.RegularExpressions;
using System.Collections.Specialized;


/*! @brief         Simple test class.
    @details    This class should help get you started. It demonstrates everything you need to start reading health data.
                To read steps in the last 24 hours, simply tap "Read data". To read sleep, uncomment the ReadSleep() call in Test().
                If you're testing in the simulator, or are okay with junking up your device with dummy data, set generateDummyData to true.
                This will un-hide another button – "[dummy data]" – tap this one to call the GenerateDummyData() method.
                If something goes wrong, it should display an error underneath the box.
 */
namespace DRDC1
{
	public class HealthKitTest : MonoBehaviour
	{

		public Text resultsLabel;        /*!< @brief to display the results of ReadData */
		//public Text errorLabel;            /*!< @brief error text, for if something goes wrong */
		public HealthKitDataTypes types;
		private TechnicalBreathing tbobject;
		private float checkrate = 2f;
		private float initcheck = 2f;
		private float nextcheck = 0;
		private float totalseconds = 3610f;
		private float threehourseconds = -10800f;
		private HealthStore healthStore;
		Regex myRegex = new Regex(@"(\d+) (\w+)/(\w+)");
		Regex myRegex1 = new Regex(@"(\d+)");
		// this is disabled by default, to prevent junking up your device with debug data, in case you use your primary phone as a test device.
		// if that's the case, it's probably better to use this in the simulator.
		void Start()
		{
			Debug.Log("---------- START ----------");
			this.healthStore = this.GetComponent<HealthStore>();
			this.tbobject = this.GetComponent<TechnicalBreathing> ();



			if (Application.platform != RuntimePlatform.IPhonePlayer)
			{
				string error = "HealthKit only works on iOS devices! It will not work in the Unity Editor.";
				//this.errorLabel.text = error;
				Debug.LogError(error);
			}
			else
			{
				this.healthStore.Authorize(this.types);

			}
		}

		void Update()
		{
			if (Time.realtimeSinceStartup <= initcheck)
			{

			}
			else
			{
				if (Time.realtimeSinceStartup > nextcheck)
				{
					nextcheck = Time.realtimeSinceStartup + checkrate;
					ReadData();
				}

			}

		}


		/*! @brief Fire off the appropriate HealthKit query.
      */
		public void ReadData()
		{
			// since this can take a while, especially on older devices, I use this to avoid firing off multiple concurrent requests.
			Debug.Log("read data...");
			HKDataType dataType = (HKDataType)Enum.Parse(typeof(HKDataType), "HKQuantityTypeIdentifierHeartRate");
			DateTimeOffset now = DateTimeOffset.Now;
			// for this example, we'll read everything from the past 24 hours
			DateTimeOffset start = now.AddSeconds(-totalseconds + threehourseconds);//change the accuracy
			ReadQuantityData(dataType, start, now);

		}


		private void ErrorOccurred(Error err)
		{
			//this.errorLabel.text = err.localizedDescription;
		}

		private void ReadQuantityData(HKDataType dataType, DateTimeOffset start, DateTimeOffset end)
		{
			string typeName = HealthKitDataTypes.GetIdentifier(dataType);
			Debug.LogFormat("reading {0} from {1} to {2}", typeName, start, end);
			double sum = 0;
			this.healthStore.ReadQuantitySamples(dataType, start, end, delegate (List<QuantitySample> samples) {
				if (samples.Count > 0)
				{
					Debug.Log("found " + samples.Count + " samples");
					bool cumulative = (samples[0].quantityType == QuantityType.cumulative);
					string text = "";
					text = text + "- " + samples[samples.Count - 1] + "\n";
					Match OneMatch = myRegex.Match(text);
					text = OneMatch.Groups[0].Value;
					Match IntMatch = myRegex1.Match(text);
					string inttext = IntMatch.Groups[0].Value;
					if (cumulative)
					{
						if (sum > 0) this.resultsLabel.text = typeName + ":" + sum;
					}
					else
					{
						//string checkrate=(tbobject.getCheckRate()).ToString();
						tbobject.receivedata(Convert.ToInt32(inttext));//transfer heart rate data to tactical breath part
						this.resultsLabel.text = inttext;
					}
				}
				else
				{
					this.resultsLabel.text = "found no samples";
					Debug.Log("found no samples");
				}
			});
		}
	}
}